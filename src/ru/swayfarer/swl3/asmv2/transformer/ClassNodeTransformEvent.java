package ru.swayfarer.swl3.asmv2.transformer;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class ClassNodeTransformEvent
{
    @Internal
    public ClassNode classNode;

    @Internal
    public int classWriterAction = -1;

    public ClassNodeTransformEvent applyClassWriterAction(int newAction)
    {
        this.classWriterAction = Math.max(newAction, classWriterAction);
        return this;
    }

    public boolean isTransformed()
    {
        return classWriterAction >= 0;
    }
}
