package ru.swayfarer.swl3.asmv2.transformer;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.ClassVisitor;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public abstract class VisitorClassNodeTransformer implements IClassNodeTransformer
{
    @NonNull
    @Internal
    public AsmUtilsV2 asmUtils;

    public VisitorClassNodeTransformer()
    {
        this(AsmUtilsV2.getInstance());
    }

    public VisitorClassNodeTransformer(@NonNull AsmUtilsV2 asmUtils)
    {
        this.asmUtils = asmUtils;
    }

    @Override
    public void transform(ClassNodeTransformEvent classNodeEvent)
    {
        var classNode = classNodeEvent.getClassNode();
        var visitor = createVisitor(classNode);

        if (visitor != null)
        {
            try
            {
                asmUtils.classes().modify(classNode, visitor);
            }
            catch (Throwable e)
            {
                throw new RuntimeException("Can't transform class " + classNode.name, e);
            }

            classNodeEvent.applyClassWriterAction(getClassWriterEvent(visitor));
        }
    }

    public int getClassWriterEvent(ClassVisitor cv)
    {
        var retAction = -1;
        var parent = cv;

        while (parent != null)
        {
            if (parent instanceof IClassWriterReportedClassVisitor)
            {
                var action = ((IClassWriterReportedClassVisitor) parent).getClassWriterActionNeeded();
                retAction = Math.max(retAction, action);
            }
            parent = parent.getParent();
        }

        return retAction;
    }

    public abstract ClassVisitor createVisitor(ClassNode classNode);
}
