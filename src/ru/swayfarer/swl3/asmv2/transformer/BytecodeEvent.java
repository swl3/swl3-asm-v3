package ru.swayfarer.swl3.asmv2.transformer;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class BytecodeEvent
{
    @Internal
    public String name;

    @Internal
    public byte[] classBytes;

    @Internal
    public int classWriterAction = -1;

    public boolean isTransformed()
    {
        return classWriterAction != -1;
    }
}
