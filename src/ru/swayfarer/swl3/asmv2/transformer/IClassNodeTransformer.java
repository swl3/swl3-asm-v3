package ru.swayfarer.swl3.asmv2.transformer;

public interface IClassNodeTransformer
{
    public void transform(ClassNodeTransformEvent classNode);
}
