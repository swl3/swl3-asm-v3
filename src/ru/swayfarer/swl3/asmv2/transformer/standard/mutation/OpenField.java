package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Marks field as needed to open. <br>
 * Use it on {@link TargetOwned} marked fields <br>
 * Removes "final" modifier from target field.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface OpenField
{
}
