package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Marks method as field-call mutation <br>
 * Field call mutations catches target field invocations <br>
 * Allows replace field get or set operations in mutation target class <br>
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MutateFieldCall
{
    /**
     * Get field value operation, as <code>System.out.println(this.myField)</code>
     */
    public static final String fieldCallGet = "get";

    /**
     * Get field value operation, as <code>this.myField = "Hello, World"</code>
     */
    public static final String fieldCallSet = "set";

    /**
     * Target field name. If not set than call mutation method's name will be used.
     */
    public String name() default "";

    /**
     * The field desc. Can be aliased by {@link #type()} parameter.
     * If not set than mutation method signature will be used
     */
    public String desc() default "";

    /**
     * The field type. Can be aliased by {@link #desc()} parameter.
     * If not set than mutation method signature will be used
     */
    public Class<?> type() default Object.class;

    /**
     * Field owner class name. Can be aliased by {@link #owner()} parameter.
     * If not set than mutation-target class will be used as field owner
     */
    public String ownerName() default "";

    /**
     * Field owner class. Can be aliased by {@link #ownerName()} parameter.
     * If not set than mutation-target class will be used as field owner
     */
    public Class<?> owner() default Object.class;

    /**
     * Field invocation type. See {@link #fieldCallGet} and {@link #fieldCallSet} <br>
     * If not set than {@link #fieldCallGet} will be used by default.
     */
    public String mode() default fieldCallGet;
}
