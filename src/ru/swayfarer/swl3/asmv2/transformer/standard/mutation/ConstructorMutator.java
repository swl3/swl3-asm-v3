package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.MethodVisitor;
import ru.swayfarer.swl3.asmv2.asm.Opcodes;
import ru.swayfarer.swl3.asmv2.asm.commons.AdviceAdapter;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.markers.Internal;

public class ConstructorMutator extends MethodVisitor implements Opcodes
{
    @Internal
    public String owner;

    @Internal
    public ExtendedList<String> mutationMethods;

    public ConstructorMutator(String owner, ExtendedList<String> mutations, MethodNode methodNode, MethodVisitor methodVisitor)
    {
        super(Opcodes.ASM_CURRENT, methodVisitor);
        this.owner = owner;
        this.mutationMethods = mutations;
    }

    @Override
    public void visitInsn(int opcode)
    {
        if (opcode == RETURN || opcode == ATHROW)
        {
            onMethodExit(opcode);
        }

        super.visitInsn(opcode);
    }

    protected void onMethodExit(int opcode)
    {
        for (var injection : mutationMethods)
        {
            visitVarInsn(ALOAD, 0);

            super.visitMethodInsn(
                    Opcodes.INVOKEVIRTUAL,
                    owner,
                    injection,
                    "()V",
                    false
            );
        }
    }
}
