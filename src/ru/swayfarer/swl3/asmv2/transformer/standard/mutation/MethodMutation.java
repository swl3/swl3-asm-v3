package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.ClassVisitor;
import ru.swayfarer.swl3.asmv2.asm.MethodVisitor;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.commons.Method;
import ru.swayfarer.swl3.asmv2.asm.commons.MethodRemapper;
import ru.swayfarer.swl3.asmv2.asm.commons.Remapper;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class MethodMutation
{
    @Internal
    @NonNull
    public MethodNode originalMethod;

    @Internal
    @NonNull
    public Type originalOwner;

    @Internal
    public ExtendedMap<String, Object> customProperties = new ExtendedMap<>();

    public void cloneContentTo(ClassVisitor cv, Type newOwner)
    {
        var mv = cv.visitMethod(originalMethod.access, originalMethod.name, originalMethod.desc, originalMethod.signature, null);
        cloneContentTo(mv, newOwner);
    }

    public void cloneContentTo(MethodVisitor mv, Type newOwner)
    {
        if (!isStaticCall())
        {
            Remapper remapper = new Remapper()
            {
                @Override
                public String map(String internalName)
                {
                    if (internalName.equals(originalOwner.getInternalName()))
                        return newOwner.getInternalName();

                    return super.map(internalName);
                }
            };

            mv = new MethodRemapper(mv, remapper);
        }

        originalMethod.accept(mv);
    }

    public boolean isInjectionPointEqual(String name)
    {
        return name.equals(getMutationPoint());
    }

    public ExtendedOptional<Integer> getTargetLine()
    {
        return ExtendedOptional.of(getCustomProperties().getValue("swl3-injection-line"));
    }

    public MethodMutation setTargetLine(Integer value)
    {
        getCustomProperties().put("swl3-injection-line", value);
        return this;
    }

    public String getMutationPoint()
    {
        return getCustomProperties().getValue("swl3-injection-point");
    }

    public boolean isStaticCall()
    {
        return Boolean.TRUE.equals(getCustomProperties().getValue("swl3-injection-static"));
    }

    public boolean hasMutateVars()
    {
        return !CollectionsSWL.isNullOrEmpty(getMutateVars());
    }

    public ExtendedList<Integer> getMutateVars()
    {
        return getCustomProperties().getValue("swl3-can-mutate-args");
    }

    public void setMutateVars(ExtendedList<Integer> indexes)
    {
        getCustomProperties().put("swl3-can-mutate-args", indexes);
    }

    public boolean hasReadVars()
    {
        return !CollectionsSWL.isNullOrEmpty(getReadVars());
    }

    public ExtendedList<Integer> getReadVars()
    {
        return getCustomProperties().getValue("swl3-read-local-args");
    }

    public void setReadVars(ExtendedList<Integer> indexes)
    {
        getCustomProperties().put("swl3-read-local-args", indexes);
    }

    public Method getMethodTarget()
    {
        return getCustomProperties().getValue("swl3-method-target");
    }

    public MethodMutation setMethodTarget(Method method)
    {
        getCustomProperties().put("swl3-method-target", method);
        return this;
    }

    public boolean hasMethodController()
    {
        return Boolean.TRUE.equals(getCustomProperties().getValue("swl3-has-method-controller"));
    }

    public MethodMutationPointTarget getCallMethodTarget()
    {
        return getCustomProperties().getValue("swl3-call-target-method");
    }

    public MethodMutation setMethodTarget(MethodMutationPointTarget target)
    {
        getCustomProperties().put("swl3-call-target-method", target);
        return this;
    }

    public MethodMutation setMutationPoint(String point)
    {
        getCustomProperties().put("swl3-injection-point", point);
        return this;
    }

    public MethodMutation setStaticCall(boolean isStaticCall)
    {
        getCustomProperties().put("swl3-injection-static", isStaticCall);
        return this;
    }

    public MethodMutation setHasController(boolean hasController)
    {
        getCustomProperties().put("swl3-has-method-controller", hasController);
        return this;
    }

    public ExtendedList<Integer> getJumpAvailableLines()
    {
        return getCustomProperties().getValue("swl3-jump-available-lines");
    }

    public MethodMutation setJumpAvailableLines(ExtendedList<Integer> lines)
    {
        getCustomProperties().put("swl3-jump-available-lines", lines);
        return this;
    }
}
