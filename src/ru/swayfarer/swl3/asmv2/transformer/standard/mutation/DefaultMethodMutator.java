package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.Label;
import ru.swayfarer.swl3.asmv2.asm.MethodVisitor;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.markers.Internal;

import java.lang.reflect.Modifier;
import java.util.UUID;

@Getter
@Setter
@Accessors(chain = true)
public class DefaultMethodMutator extends MethodMutationVisitor
{
    @Internal
    public ExtendedMap<UUID, Integer> repeatCountLeft = new ExtendedMap<>();

    @Internal
    public ExtendedMap<Integer, Label> jumpToLineLabels = new ExtendedMap<>();

    @Internal
    public boolean injecting;

    @Internal
    public ICachedMutations<MethodMutation> mutationsStreamFun = ICachedMutations.of(() ->
        getMutationConfig().findMethodMutationsFor(Type.getObjectType(getClassNode().name))
            .filter((entry) -> {
                var methodNode = getMethodNode();
                var methodTarget = entry.getMutation().getMethodTarget();

                if (!methodTarget.getName().equals(methodNode.name))
                    return false;

                if (!methodNode.desc.startsWith(methodTarget.getDescriptor()))
                    return false;

                return true;
            })
    );

    @Internal
    public ICachedMutations<MethodMutation> lineMutationsStreamFun = ICachedMutations.of(() ->
            getMutationsStreamFun().apply()
                .filter(entry -> entry.getMutation().isInjectionPointEqual(MutationPoints.Line))
    );

    @Internal
    public ICachedMutations<MethodMutation> methodCallMutationsStreamFun = ICachedMutations.of(() ->
            getMutationsStreamFun().apply()
                .filter(entry -> entry.getMutation().isInjectionPointEqual(MutationPoints.MethodCall))
    );

    public DefaultMethodMutator(
            MutationConfig mutationConfig,
            ClassMutationsVisitor classMutationsVisitor,
            MethodNode methodNode,
            MethodVisitor methodVisitor
    )
    {
        super(mutationConfig, classMutationsVisitor, methodNode, methodVisitor);
    }

    @Override
    protected void onMethodEnter()
    {
        mutationsStreamFun.apply()
                .each((entry) -> {
                    var mutation = entry.getMutation();

                    if (mutation.isInjectionPointEqual(MutationPoints.Enter))
                    {
                        injectMutationCall(entry);
                    }
                })
        ;

        super.onMethodEnter();
    }

    @Override
    public void visitMethodInsn(int opcodeAndSource, String owner, String name, String descriptor, boolean isInterface)
    {
        var methodMutationsList = new ExtendedList<MutationEntry<MethodMutation>>();

        methodCallMutationsStreamFun.apply()
                .each((entry) -> {
                    var mutation = entry.getMutation();
                    var methodTarget = mutation.getCallMethodTarget();

                    var targetMethod = methodTarget.getMethod();
                    var targetMethodOwner = methodTarget.getOwner();

                    if (targetMethod != null)
                    {
                        var b1 = targetMethodOwner == null || targetMethodOwner.getInternalName().equals(owner);
                        var b2 = targetMethod.getName().equals(name);
                        var b3 = descriptor.startsWith(targetMethod.getDescriptor());

                        if (b1 && b2 && b3)
                        {
                            methodMutationsList.add(entry);
                        }
                    }
                })
        ;

        for (var entry : methodMutationsList)
        {
            var mutation = entry.getMutation();
            var methodTarget = mutation.getCallMethodTarget();

            if (methodTarget.getStage().equals(MethodCallStage.MutateBefore))
            {
                if (decrRepeatLeft(entry))
                    injectMutationCall(entry);
            }
        }

        super.visitMethodInsn(opcodeAndSource, owner, name, descriptor, isInterface);

        for (var entry : methodMutationsList)
        {
            var mutation = entry.getMutation();
            var methodTarget = mutation.getCallMethodTarget();

            if (methodTarget.getStage().equals(MethodCallStage.MutateAfter))
            {
                if (decrRepeatLeft(entry))
                    injectMutationCall(entry);
            }
        }
    }

    public boolean decrRepeatLeft(MutationEntry<MethodMutation> entry)
    {
        var repeatCount = repeatCountLeft.getOrCreate(
                entry.getTrackId(),
                () -> entry.getMutation().getCallMethodTarget().getRepeatCount()
        );

        if (repeatCount == MutationRepeat.MutateAll)
            return true;

        if (repeatCount > 0)
        {
            repeatCountLeft.put(entry.getTrackId(), repeatCount - 1);
            return true;
        }

        return false;
    }

    @Override
    public void onMethodExit(int opcode)
    {
        mutationsStreamFun.apply()
                .each((entry) -> {
                    var mutation = entry.getMutation();

                    if (mutation.isInjectionPointEqual(MutationPoints.Exit))
                    {
                        injectMutationCall(entry, opcode);
                    }
                })
        ;

        super.onMethodExit(opcode);
    }

    @Override
    public void visitLineNumber(int line, Label start)
    {
        var lbl = jumpToLineLabels.get(line);

        if (lbl != null)
        {
            visitLabel(lbl);
        }

        lineMutationsStreamFun.apply()
                .each((entry) -> {
                    var mutation = entry.getMutation();

                    if (mutation.isInjectionPointEqual(MutationPoints.Line))
                    {
                        var mutationLine = mutation.getTargetLine().orElse(-1);

                        if (line == mutationLine)
                            injectMutationCall(entry);
                    }
                })
        ;

        super.visitLineNumber(line, start);
    }

    public void injectMutationCall(MutationEntry<MethodMutation> entry)
    {
        injectMutationCall(entry, -1);
    }

    @Override
    public void visitCode()
    {
        mutationsStreamFun.apply()
                .each((mutationEntry) -> {
                    var mutation = mutationEntry.getMutation();
                    var jumpAvailableLines = mutation.getJumpAvailableLines();
                    var jumpsToLinesEnabled = !CollectionsSWL.isNullOrEmpty(jumpAvailableLines);

                    if (jumpsToLinesEnabled)
                    {
                        for (var line : jumpAvailableLines)
                        {
                            jumpToLineLabels.getOrCreate(line, Label::new);
                        }
                    }
                })
        ;

        super.visitCode();
    }

    public void injectMutationCall(MutationEntry<MethodMutation> entry, int exitOpcode)
    {
        if (isInjecting())
            return;

        setInjecting(true);

        var logMethodName = classNode.name + "::" + methodNode.name + methodNode.desc;
        var classType = Type.getObjectType(classNode.name);
        var mutation = entry.getMutation();
        var controllerType = Type.getType(MethodController.class);
        var hasMethodController = mutation.hasMethodController();
        var methodControllerVarId = -1;
        var isThrow = exitOpcode == ATHROW;
        var isExiting = exitOpcode != -1;
        var oldReturnVarId = -1;
        var methodReturnType = Type.getReturnType(methodNode.desc);
        var methodControllerType = Type.getType(MethodController.class);
        var controllerHelperType = Type.getType(MethodControllerHelper.class);
        var jumpAvailableLines = mutation.getJumpAvailableLines();
        var jumpsToLinesEnabled = !CollectionsSWL.isNullOrEmpty(jumpAvailableLines);

        // If exiting (return or throw) than store old exit value (retVal or throwable)
        if (isExiting)
        {
            if (isThrow)
            {
                oldReturnVarId = newLocal(Type.getType(Throwable.class));
                visitVarInsn(ASTORE, oldReturnVarId);
            }
            else if (!Type.VOID_TYPE.equals(methodReturnType))
            {
                oldReturnVarId = newLocal(methodReturnType);
                visitVarInsn(methodReturnType.getOpcode(ISTORE), oldReturnVarId);
            }
        }

        if (hasMethodController)
        {
            methodControllerVarId = newLocal(methodControllerType);

            visitTypeInsn(NEW, methodControllerType.getInternalName());
            visitInsn(DUP);
            visitMethodInsn(INVOKESPECIAL, controllerType.getInternalName(), "<init>", "()V", false);
            visitVarInsn(ASTORE, methodControllerVarId);

            if (!Modifier.isStatic(methodNode.access))
            {
                visitVarInsn(ALOAD, methodControllerVarId);
                loadThis();

                visitMethodInsn(
                        INVOKESTATIC,
                        controllerHelperType.getInternalName(),
                        "setInvocationInstance",
                        "(" + methodControllerType.getDescriptor() + "Ljava/lang/Object;)V",
                        false
                );
            }

            if (mutation.hasReadVars())
            {
                var maxLocalId = this.nextLocal;
                visitVarInsn(ALOAD, methodControllerVarId);
                visitMethodInsn(
                        INVOKEVIRTUAL,
                        controllerType.getInternalName(),
                        "isLocalVarsRead",
                        "()Z",
                        false
                );

                var lblIfNotVarsReaded = newLabel();
                visitJumpInsn(IFEQ, lblIfNotVarsReaded);

                for (var localVarToReadId : mutation.getReadVars())
                {
                    ExceptionsUtils.If(
                            localVarToReadId >= maxLocalId,
                            IllegalStateException.class,
                            "Mutation method ", logMethodName, " tried to read local var with index", localVarToReadId, " but its does not exists!"
                    );

                    visitVarInsn(ALOAD, methodControllerVarId);

                    visitLdcInsn(localVarToReadId);

                    var localType = getLocalVarType(localVarToReadId);
                    visitVarInsn(localType.getOpcode(ILOAD), localVarToReadId);
                    asmUtils.types().conversions()
                            .convert(this, localType, Type.getType(Object.class))
                    ;

                    visitMethodInsn(
                            INVOKEVIRTUAL,
                            controllerType.getInternalName(),
                            "putLocalVar",
                            "(ILjava/lang/Object;)V",
                            false
                    );
                }

                visitLabel(lblIfNotVarsReaded);
            }

            if (isExiting)
            {
                if (isThrow)
                {
                    visitVarInsn(ALOAD, methodControllerVarId);
                    visitVarInsn(ALOAD, oldReturnVarId);

                    // Invoke MethodControllerHelper::setException method if exit with throw
                    visitMethodInsn(
                            INVOKESTATIC,
                            controllerHelperType.getInternalName(),
                            "setException",
                            "(" + methodControllerType.getDescriptor() + "Ljava/lang/Throwable;)V",
                            false
                    );
                }
                else if (!Type.VOID_TYPE.equals(methodReturnType))
                {
                    visitVarInsn(ALOAD, methodControllerVarId);
                    visitVarInsn(methodReturnType.getOpcode(ILOAD), oldReturnVarId);

                    // Invoke MethodControllerHelper::setReturnValue method if exit with return
                    visitMethodInsn(
                            INVOKESTATIC,
                            controllerHelperType.getInternalName(),
                            "setReturnValue",
                            "(" + methodControllerType.getDescriptor() + "Ljava/lang/Object;)V",
                            false
                    );
                }
            }
        }
        var staticInvocation = Modifier.isStatic( mutation.originalMethod.access);
        var staticMutationCall = mutation.isStaticCall();

        var mutationOriginalMethod = mutation.getOriginalMethod();

        var newMethodName = staticMutationCall ? mutationOriginalMethod.name : cloneMethodMutation(classType,
                Type.getObjectType(entry.mutationHolder.name),
                mutation,
                mutationOriginalMethod
        );

        var invokeOpcode = staticInvocation ? INVOKESTATIC : INVOKEVIRTUAL;
        var invokeOwner = staticMutationCall ? mutation.getOriginalOwner().getInternalName() : classNode.name;

        if (!staticInvocation)
            loadThis();

        loadArgs();

        if (hasMethodController)
            visitVarInsn(ALOAD, methodControllerVarId);

        validateStatic(methodNode, mutation);
        visitMethodInsn(
                invokeOpcode,
                invokeOwner,
                newMethodName,
                mutationOriginalMethod.desc,
                false
        );

        if (hasMethodController)
        {
            var lblIfNotReturn = newLabel();
            visitVarInsn(ALOAD, methodControllerVarId);
            visitMethodInsn(
                    INVOKEVIRTUAL,
                    controllerType.getInternalName(),
                    "isReturnTouched",
                    "()Z",
                    false
            );

            visitJumpInsn(IFEQ, lblIfNotReturn);

            if (Type.VOID_TYPE.equals(methodReturnType))
            {
                visitInsn(RETURN);
            }
            else
            {
                visitVarInsn(ALOAD, methodControllerVarId);

                visitMethodInsn(
                        INVOKEVIRTUAL,
                        controllerType.getInternalName(),
                        "getReturnValue",
                        "()Ljava/lang/Object;",
                        false
                );

                asmUtils.types().conversions().convert(
                        this,
                        Type.getType(Object.class),
                        methodReturnType
                );

                visitInsn(methodReturnType.getOpcode(IRETURN));
            }

            visitLabel(lblIfNotReturn);

            if (jumpsToLinesEnabled)
            {
                visitVarInsn(ALOAD, methodControllerVarId);
                visitMethodInsn(
                        INVOKEVIRTUAL,
                        controllerType.getInternalName(),
                        "isLineJumpApplied",
                        "()Z",
                        false
                );

                var lblIfNotEnabled = newLabel();
                visitJumpInsn(IFEQ, lblIfNotEnabled);

                var jumpLineVarId = newLocal(Type.INT_TYPE);
                visitVarInsn(ALOAD, methodControllerVarId);
                visitMethodInsn(
                        INVOKEVIRTUAL,
                        controllerType.getInternalName(),
                        "getLineToJump",
                        "()I",
                        false
                );
                visitVarInsn(ISTORE, jumpLineVarId);

                for (var availableLine : jumpAvailableLines)
                {
                    visitVarInsn(ILOAD, jumpLineVarId);
                    visitLdcInsn(availableLine);

                    var lblIfNotThisLine = newLabel();
                    visitJumpInsn(IF_ICMPNE, lblIfNotThisLine);

                    visitInsn(ICONST_0);
                    visitJumpInsn(IFEQ, jumpToLineLabels.get(availableLine));

                    visitLabel(lblIfNotThisLine);
                }

                visitLabel(lblIfNotEnabled);
            }
        }

        if (isExiting)
        {
            if (isThrow)
                visitVarInsn(ALOAD, oldReturnVarId);
            else if (!methodReturnType.equals(Type.VOID_TYPE))
                visitVarInsn(methodReturnType.getOpcode(ILOAD), oldReturnVarId);
        }
        else
        {
            // Change local vars values if needed
            if (mutation.hasMutateVars())
            {
                visitVarInsn(ALOAD, methodControllerVarId);
                visitMethodInsn(
                        INVOKEVIRTUAL,
                        controllerType.getInternalName(),
                        "isLocalVarsChanged",
                        "()Z",
                        false
                );

                var lblIfNotAllVarsChanged = newLabel();
                visitJumpInsn(IFEQ, lblIfNotAllVarsChanged);

                var maxLocalId = this.nextLocal;

                for (var localVarToChangeId : mutation.getMutateVars())
                {
                    ExceptionsUtils.If(
                            localVarToChangeId >= maxLocalId,
                            IllegalStateException.class,
                            "Mutation method ", logMethodName, " tried to change local var with index", localVarToChangeId, " but its does not exists!"
                    );

                    var varType = getLocalVarType(localVarToChangeId);

                    visitVarInsn(ALOAD, methodControllerVarId);
                    visitLdcInsn(localVarToChangeId);
                    visitMethodInsn(
                            INVOKEVIRTUAL,
                            controllerType.getInternalName(),
                            "isLocalVarChanged",
                            "(I)Z",
                            false
                    );

                    var lblIfNotThisLocalVarChanged = newLabel();
                    visitJumpInsn(IFEQ, lblIfNotThisLocalVarChanged);

                    visitVarInsn(ALOAD, methodControllerVarId);
                    visitLdcInsn(localVarToChangeId);
                    visitMethodInsn(
                            INVOKEVIRTUAL,
                            controllerType.getInternalName(),
                            "getNewVarValue",
                            "(I)Ljava/lang/Object;",
                            false
                    );

                    asmUtils.types().conversions().convert(this, Type.getType(Object.class), varType);

                    visitVarInsn(varType.getOpcode(ISTORE), localVarToChangeId);
                    visitLabel(lblIfNotThisLocalVarChanged);
                }

                visitLabel(lblIfNotAllVarsChanged);
            }
        }

        setInjecting(false);
    }

    public Type getLocalVarType(int index)
    {
        if (index < firstLocal)
        {
            if (!Modifier.isStatic(methodNode.access) && index == 0)
            {
                return Type.getObjectType(classNode.name);
            }

            return getArgumentTypes()[index];
        }

        return getLocalType(index);
    }

    public void validateStatic(MethodNode sourceNode, MethodMutation mutation)
    {
        var mutationMethod = mutation.getOriginalMethod();
        var logName = mutation.getOriginalOwner().getClassName() + "::" + mutationMethod.name;

        if (Modifier.isStatic(sourceNode.access) && !Modifier.isStatic(mutationMethod.access))
            ExceptionsUtils.throwException(new IllegalStateException("Can't invoke non-static mutation method " + logName + " from static target!"));
    }

    public String cloneMethodMutation(Type classType, Type mutationOwner, MethodMutation mutation, MethodNode mutationOriginalMethod)
    {
        var newMethodName = mutationConfig.getRandomMutationName(
                "method",
                mutationOwner.getInternalName(),
                mutationOriginalMethod.name
        );

        var mmv = classMutationsVisitor.visitMethod(
                mutationOriginalMethod.access,
                newMethodName,
                mutationOriginalMethod.desc,
                mutationOriginalMethod.signature,
                null
        );

        mutation.cloneContentTo(mmv, classType);

        return newMethodName;
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @SuperBuilder
    public static class FutureLabelEntry {
        public int lineNum;
        public Label lbl;
    }
}
