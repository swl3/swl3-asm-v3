package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.commons.Method;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class MethodMutationPointTarget
{
    @Internal
    public Type owner;

    @Internal
    @NonNull
    public Method method;

    @Internal
    @NonNull
    public int repeatCount;

    @Internal
    @NonNull
    public MethodCallStage stage = MethodCallStage.MutateBefore;

}
