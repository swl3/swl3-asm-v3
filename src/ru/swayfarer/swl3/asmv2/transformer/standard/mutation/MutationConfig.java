package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;

import java.util.UUID;

@Getter
@Setter
@Accessors(chain = true)
public class MutationConfig
{
    public String prefix = "__$swl3_mutation_";

    @Internal
    @NonNull
    public ExtendedList<MutationEntry<FieldMutation>> newFields = new ExtendedList<>();

    @Internal
    @NonNull
    public ExtendedList<MutationEntry<MethodMutation>> methodMutations = new ExtendedList<>();

    @Internal
    @NonNull
    public ExtendedList<MutationEntry<MethodMutation>> constructorMutations = new ExtendedList<>();

    @Internal
    @NonNull
    public ExtendedList<MutationEntry<MethodMutation>> classInitMutations = new ExtendedList<>();

    @Internal
    @NonNull
    public ExtendedList<MutationEntry<MethodMutation>> newMethods = new ExtendedList<>();

    @Internal
    @NonNull
    public ExtendedList<MutationEntry<TypeMutation>> typeMutations = new ExtendedList<>();

    @Internal
    @NonNull
    public ExtendedList<MutationEntry<OpenFieldsMutation>> openFieldsMutations = new ExtendedList<>();

    @Internal
    @NonNull
    public ExtendedList<MutationEntry<FieldCallMutation>> fieldCallMutations = new ExtendedList<>();

    @Internal
    @NonNull
    public ExtendedMap<String, ExtendedList<MutationEntry<?>>> mutationsCache = new ExtendedMap<>().synchronize();

    public ExtendedStream<MutationEntry<TypeMutation>> findTypeMutationsFor(Type type)
    {
        return findMutationsFor(type, typeMutations, "__types");
    }

    public ExtendedStream<MutationEntry<FieldMutation>> findNewFieldsFor(Type type)
    {
        return findMutationsFor(type, newFields, "__newFields");
    }

    public ExtendedStream<MutationEntry<MethodMutation>> findClassInitMutationsFor(Type type)
    {
        return findMutationsFor(type, classInitMutations, "__clinit");
    }

    public ExtendedStream<MutationEntry<MethodMutation>> findMethodMutationsFor(Type type)
    {
        return findMutationsFor(type, methodMutations, "__mutations");
    }

    public ExtendedStream<MutationEntry<MethodMutation>> findConstructorMutationsFor(Type type)
    {
        return findMutationsFor(type, constructorMutations, "__constructors");
    }

    public ExtendedStream<MutationEntry<MethodMutation>> findNewMethodsFor(Type type)
    {
        return findMutationsFor(type, newMethods, "__newMethods");
    }

    public ExtendedStream<MutationEntry<OpenFieldsMutation>> findOpenFieldsFor(Type type)
    {
        return findMutationsFor(type, openFieldsMutations, "__openFields");
    }

    public ExtendedStream<MutationEntry<FieldCallMutation>> findFieldCallMutationsFor(Type type)
    {
        return findMutationsFor(type, fieldCallMutations, "__fieldCallMutations");
    }

    @Internal
    public <T> ExtendedStream<MutationEntry<T>> findMutationsFor(Type type, Iterable<MutationEntry<T>> content, String cacheKey)
    {
        var key = type.getInternalName().concat(cacheKey);

        var result = mutationsCache.getOrCreate(key, () -> {
            var list = ExtendedStream.of(content, false)
                    .filter((e) -> e.targetFilters.apply(type))
                    .toExList()
            ;

            return ReflectionsUtils.cast(list);
        }).exStream();

        return ReflectionsUtils.cast(result);
    }

    public boolean hasAnyMutations(Type classNode)
    {
        if (findMethodMutationsFor(classNode).findAny().isPresent())
            return true;

        if (findNewMethodsFor(classNode).findAny().isPresent())
            return true;

        if (findNewFieldsFor(classNode).findAny().isPresent())
            return true;

        if (findClassInitMutationsFor(classNode).findAny().isPresent())
            return true;

        if (findConstructorMutationsFor(classNode).findAny().isPresent())
            return true;

        if (findTypeMutationsFor(classNode).findAny().isPresent())
            return true;

        if (findOpenFieldsFor(classNode).findAny().isPresent())
            return true;

        if (findFieldCallMutationsFor(classNode).findAny().isPresent())
            return true;

        return false;
    }

    public String getRandomMutationName(String name, String mutationOwnerName, String mutationEntryName)
    {
        if (mutationEntryName.equals("<init>"))
            mutationEntryName = "$constructor";

        if (mutationEntryName.equals("<clinit>"))
            mutationEntryName = "$classInit";

        var mutationOwner = ReflectionsUtils.getShortedClassName(mutationOwnerName.replace("/", "."), 1);
        mutationOwner = mutationOwner.replace('.', '_');
        var mutationLocation = mutationOwner + "_" + mutationEntryName;

        return
                StringUtils.orEmpty(prefix) +
                "__" +
                name +
                "__" +
                mutationLocation +
                "__" +
                UUID.randomUUID().toString().replace("-", "")
        ;
    }
}
