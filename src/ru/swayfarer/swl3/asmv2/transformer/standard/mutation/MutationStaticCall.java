package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Mark mutation method as static mutation <br>
 * Static mutations calls original mutation methods (which marked by this annotation) <br>
 * <h3> Note that "Static mutations" are not equal "Static methods" </h3>
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MutationStaticCall
{
}
