package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.asmv2.asm.Opcodes;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.commons.Method;
import ru.swayfarer.swl3.asmv2.asm.tree.AnnotationNode;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.asm.tree.FieldNode;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.ExceptionOptional;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

import java.lang.reflect.Modifier;
import java.util.HashSet;

@Getter
@Setter
@Accessors(chain = true)
public class MutationConfigReader
{
    @Internal
    public MutationConfig resultConfig;

    @Internal
    @NonNull
    public AsmUtilsV2 asmUtils;

    @Internal
    public ILogger logger = LogFactory.getLogger();

    public MutationConfigReader(AsmUtilsV2 asmUtils)
    {
        this(asmUtils, new MutationConfig());
    }

    public MutationConfigReader(AsmUtilsV2 asmUtils, MutationConfig resultConfig)
    {
        this.resultConfig = resultConfig;
        this.asmUtils = asmUtils;
    }

    public void readMutations(Class<?> type)
    {
        readMutations(Type.getType(type));
    }

    public void readMutations(Type type)
    {
        asmUtils.classes().findByType(type)
                .ifPresent(this::readMutations)
        ;
    }

    public void readMutations(ClassNode classNode)
    {
        var mutateClassAnnotation = asmUtils.annotations().findByType(MutateClass.class, classNode).first();

        if (mutateClassAnnotation != null)
        {
            var targetFilters = readTargetTypeFilters(mutateClassAnnotation);

            readOpenFieldMutations(classNode, targetFilters);
            readTypeMutations(classNode, targetFilters);
            readNewMethods(classNode, targetFilters);
            readNewFields(classNode, targetFilters);


            readFieldCallMutations(classNode, targetFilters);
            readConstructorMutations(classNode, targetFilters);
            readMutationMethods(classNode, targetFilters);
            readClassInitMutations(classNode, targetFilters);
        }
    }

    public void readNewFields(ClassNode classNode, FunctionsChain1<Type, Boolean> targetFilters)
    {
        var ff = asmUtils.fields().filters();
        asmUtils.fields().stream(classNode)
                .not().filter(this::isTargetVirtualField)
                .each((fieldNode) -> {
                    var fieldMutation = new FieldMutation();
                    fieldMutation.originalField = fieldNode;
                    fieldMutation.originalOwner = Type.getObjectType(classNode.name);

                    var mutationEntry = new MutationEntry<FieldMutation>();
                    mutationEntry.setMutation(fieldMutation);
                    mutationEntry.setTargetFilters(targetFilters::apply);
                    mutationEntry.setMutationHolder(classNode);

                    resultConfig.getNewFields().add(mutationEntry);
                })
        ;
    }

    public boolean isTargetVirtualField(FieldNode fieldNode)
    {
        var ff = asmUtils.fields().filters();
        return Boolean.TRUE.equals(
                ff.annotations().contains(
                        TargetOwned.class,
                        OpenField.class
                )
                .apply(fieldNode)
        );
    }

    public void readTypeMutations(ClassNode classNode, FunctionsChain1<Type, Boolean> targetFilters)
    {
        var result = new TypeMutation();
        var anyMutation = false;
        var modSafeClassNode = asmUtils.classes().checkModifySafeCache(classNode);

        if (modSafeClassNode.interfaces != null)
        {
            result.getNewInterfaces().addAll(
                    ExtendedStream.of(modSafeClassNode.interfaces)
                            .map(Type::getObjectType)
                            .toExList()
            );

            anyMutation = true;
        }

        if (anyMutation)
        {
            var mutationEntry = new MutationEntry<TypeMutation>();
            mutationEntry.setTargetFilters(targetFilters::apply);
            mutationEntry.setMutationHolder(classNode);
            mutationEntry.setMutation(result);

            resultConfig.getTypeMutations().add(mutationEntry);
        }
    }

    public void readNewMethods(ClassNode classNode, FunctionsChain1<Type, Boolean> targetFilters)
    {
        var mf = asmUtils.methods().filters();

        asmUtils.methods().stream(classNode)
                .not().filter(mf.annotations().contains(TargetOwned.class))
                .not().filter(mf.annotations().contains(MutateMethod.class))
                .not().filter(mf.annotations().contains(MutateFieldCall.class))
                .not().filter(mf.constructors())
                .not().filter(mf.classInits())

                .each((methodNode) -> {
                    var methodMutation = new MethodMutation();
                    methodMutation.originalMethod = methodNode;
                    methodMutation.originalOwner = Type.getObjectType(classNode.name);

                    var mutationEntry = new MutationEntry<MethodMutation>();
                    mutationEntry.setMutation(methodMutation);
                    mutationEntry.setTargetFilters(targetFilters::apply);
                    mutationEntry.setMutationHolder(classNode);

                    resultConfig.getNewMethods().add(mutationEntry);
                })
        ;
    }

    public void readConstructorMutations(ClassNode classNode, FunctionsChain1<Type, Boolean> targetFilters)
    {
        var mf = asmUtils.methods().filters();

        asmUtils.methods().stream(classNode)
                .filter(mf.constructors())
                .each((methodNode) -> {
                    var mutation = new MethodMutation();
                    mutation.originalMethod = methodNode;
                    mutation.originalOwner = Type.getObjectType(classNode.name);

                    var mutationEntry = new MutationEntry<MethodMutation>();
                    mutationEntry.setMutation(mutation);
                    mutationEntry.setTargetFilters(targetFilters::apply);
                    mutationEntry.setMutationHolder(classNode);

                    resultConfig.getConstructorMutations().add(mutationEntry);
                })
        ;
    }

    public void readClassInitMutations(ClassNode classNode, FunctionsChain1<Type, Boolean> targetFilters)
    {
        var mf = asmUtils.methods().filters();

        asmUtils.methods().stream(classNode)
                .filter(mf.classInits())
                .each((methodNode) -> {
                    var mutation = new MethodMutation();
                    mutation.originalMethod = methodNode;
                    mutation.originalOwner = Type.getObjectType(classNode.name);

                    var mutationEntry = new MutationEntry<MethodMutation>();
                    mutationEntry.setMutation(mutation);
                    mutationEntry.setTargetFilters(targetFilters::apply);
                    mutationEntry.setMutationHolder(classNode);

                    resultConfig.getClassInitMutations().add(mutationEntry);
                })
        ;
    }

    public void readMutationMethods(ClassNode classNode, FunctionsChain1<Type, Boolean> targetFilters)
    {
        var mf = asmUtils.methods().filters();

        asmUtils.methods().stream(classNode)
                .filter(mf.annotations().contains(MutateMethod.class))
                .not().filter(mf.annotations().contains(TargetOwned.class))
                .not().filter(mf.constructors())
                .each((methodNode) -> {
                    var methodMutation = new MethodMutation();
                    var mutateAnnotation = asmUtils.annotations().findByType(MutateMethod.class, methodNode).first();
                    var injectionPoint = readTargetInjectionPoint(mutateAnnotation);

                    var staticCall = asmUtils.annotations().findByType(MutationStaticCall.class, methodNode).isNotEmpty();

                    if (staticCall && !Modifier.isStatic(methodNode.access))
                        throw new IllegalStateException("Mutation method " + classNode.name + "::" + methodNode.name + methodNode.desc + " marked for static call but its modifier is not static!");

                    readLocalVarsAccess(methodNode, methodMutation);

                    var jumpToLineAnnotation = asmUtils.annotations().findByType(AccessJumpToLines.class, methodNode).first();

                    if (jumpToLineAnnotation != null)
                    {
                        var lines = asmUtils.annotations().properties()
                                .getList(jumpToLineAnnotation, "lines", Integer.class)
                                .orNull()
                        ;

                        if (lines != null)
                        {
                            methodMutation.setJumpAvailableLines(lines);
                        }
                    }

                    methodMutation.setOriginalOwner(Type.getObjectType(classNode.name));
                    methodMutation.setOriginalMethod(methodNode);
                    methodMutation.setMutationPoint(injectionPoint);
                    methodMutation.setStaticCall(staticCall);
                    methodMutation.setHasController(readHasController(methodNode));
                    methodMutation.setMethodTarget(readTargetMethod(methodNode, mutateAnnotation));

                    // If method mutation point is MethodCall than we need to read additional info about call target
                    if (MutationPoints.MethodCall.equals(injectionPoint))
                    {
                        var methodTarget = readTargetCallMethod(classNode, methodNode);
                        methodMutation.setMethodTarget(methodTarget);
                    }

                    // If method mutation point is MethodCall than we need to read additional info about call target
                    if (MutationPoints.Line.equals(injectionPoint))
                    {
                        methodMutation.setTargetLine(readTargetMethodLine(classNode, methodNode));
                    }

                    var mutationEntry = new MutationEntry<MethodMutation>();
                    mutationEntry.setTargetFilters(targetFilters::apply);
                    mutationEntry.setMutationHolder(classNode);
                    mutationEntry.setMutation(methodMutation);

                    resultConfig.getMethodMutations().add(mutationEntry);
                })
        ;
    }

    private void readLocalVarsAccess(MethodNode methodNode, MethodMutation methodMutation)
    {
        var localVarsMutationAnnotation = asmUtils.annotations().findByType(AccessLocalVars.class, methodNode).first();
        if (localVarsMutationAnnotation != null)
        {
            var writeIndexes = asmUtils.annotations().properties()
                    .getList(localVarsMutationAnnotation, "writeIndexes", Integer.class)
                    .orNull()
            ;

            var readIndexes = asmUtils.annotations().properties()
                    .getList(localVarsMutationAnnotation, "readIndexes", Integer.class)
                    .orNull()
            ;

            methodMutation.setReadVars(readIndexes);
            methodMutation.setMutateVars(writeIndexes);
        }
    }

    public void readOpenFieldMutations(ClassNode classNode, FunctionsChain1<Type, Boolean> targetFilters)
    {
        var ff = asmUtils.fields().filters();

        var fieldsToOpen = asmUtils.fields().stream(classNode)
                .filter(ff.annotations().contains(OpenField.class))
                .map((fieldNode) -> new OpenFieldsMutation.FieldEntry(fieldNode.desc, fieldNode.name))
                .toExList()
        ;

        if (!CollectionsSWL.isNullOrEmpty(fieldsToOpen))
        {
            var fieldOpenMutation = new OpenFieldsMutation();
            fieldOpenMutation.setFields(fieldsToOpen);

            var mutationEntry = new MutationEntry<OpenFieldsMutation>();
            mutationEntry.setMutation(fieldOpenMutation);
            mutationEntry.setMutationHolder(classNode);
            mutationEntry.setTargetFilters(targetFilters::apply);

            resultConfig.getOpenFieldsMutations().add(mutationEntry);
        }
    }

    public void readFieldCallMutations(ClassNode classNode, FunctionsChain1<Type, Boolean> targetFilters)
    {
        var mf = asmUtils.methods().filters();

        asmUtils.methods().stream(classNode)
                .filter(mf.annotations().contains(MutateFieldCall.class))
                .each((methodNode) -> {
                    var fieldCallMutation = new FieldCallMutation();
                    var mutationAnnotation = asmUtils.annotations().findByType(MutateFieldCall.class, methodNode).first();

                    var mode = asmUtils.annotations().properties()
                            .get(mutationAnnotation, "mode", String.class)
                            .orElse(MutateFieldCall.fieldCallGet)
                    ;

                    fieldCallMutation.setMutationMethod(methodNode);
                    fieldCallMutation.setMode(mode);

                    // Read field owner param
                    {
                        {
                            var ownerType = asmUtils.annotations().properties()
                                    .get(mutationAnnotation, "owner", Type.class).orNull()
                            ;

                            if (ownerType != null)
                            {
                                fieldCallMutation.setOwner(ownerType);
                            }
                        }

                        if (fieldCallMutation.getOwner() == null)
                        {
                            var ownerName = asmUtils.annotations().properties()
                                    .get(mutationAnnotation, "ownerName", String.class)
                                    .orNull()
                            ;

                            if (!StringUtils.isBlank(ownerName))
                            {
                                fieldCallMutation.setOwner(asmUtils.types().findObjectTypeByName(ownerName));
                            }
                        }
                    }

                    // Read field type param
                    {
                        {
                            var type = asmUtils.annotations().properties()
                                    .get(mutationAnnotation, "type", Type.class)
                                    .orNull()
                            ;

                            if (type != null)
                            {
                                fieldCallMutation.setFieldType(type);
                            }
                        }

                        if (fieldCallMutation.getFieldType() == null)
                        {
                            var desc = asmUtils.annotations().properties()
                                    .get(mutationAnnotation, "desc", String.class)
                                    .orNull()
                            ;

                            if (!StringUtils.isBlank(desc))
                            {
                                fieldCallMutation.setFieldType(Type.getType(desc));
                            }
                        }

                        if (fieldCallMutation.getFieldType() == null)
                        {
                            var logMethodName = classNode.name + "::" + methodNode.name + methodNode.desc;

                            if (mode.equals(MutateFieldCall.fieldCallGet))
                            {
                                var methodReturnType = Type.getReturnType(methodNode.desc);

                                ExceptionsUtils.If(
                                        methodReturnType.equals(Type.VOID_TYPE),
                                        IllegalStateException.class,
                                        "Can't automatically load target field type from set-field mutation. Return type can't be void!", logMethodName
                                );

                                fieldCallMutation.setFieldType(methodReturnType);
                            }
                            else
                            {
                                var args = Type.getArgumentTypes(methodNode.desc);

                                ExceptionsUtils.If(
                                        CollectionsSWL.isNullOrEmpty(args),
                                        IllegalStateException.class,
                                        "Can't automatically load target field type from get-field mutation. Method must have only one parameter!", logMethodName
                                );

                                fieldCallMutation.setFieldType(Type.getArgumentTypes(methodNode.desc)[0]);
                            }
                        }
                    }

                    var name = asmUtils.annotations().properties()
                            .get(mutationAnnotation, "name", String.class)
                            .orElse(methodNode.name)
                    ;

                    fieldCallMutation.setFieldName(name);
                    fieldCallMutation.setOriginalOwner(Type.getObjectType(classNode.name));

                    var mutationEntry = new MutationEntry<FieldCallMutation>();
                    mutationEntry.setMutationHolder(classNode);
                    mutationEntry.setMutation(fieldCallMutation);
                    mutationEntry.setTargetFilters(targetFilters::apply);

                    resultConfig.getFieldCallMutations().add(mutationEntry);
                })
        ;
    }

    public Method readTargetMethod(MethodNode methodNode, AnnotationNode mutateMethodAnnotation)
    {
        var name = asmUtils.annotations().properties()
                .get(mutateMethodAnnotation, "name", String.class)
                .orElse(methodNode.name)
        ;

        var desc = asmUtils.annotations().properties()
                .get(mutateMethodAnnotation, "desc", String.class)
                .orNull()
        ;

        if (StringUtils.isBlank(desc))
        {
            var buffer = new DynamicString();
            buffer.append("(");

            asmUtils.methods().parameters().stream(methodNode)
                    .filter((param) -> !skipParamOnTargetComputing(Type.getType(param.variable.desc)))
                    .each((param) -> {
                        buffer.append(param.variable.desc);
                    })
            ;

            buffer.append(")");

            desc = buffer.toString();
        }

        return new Method(name, desc);
    }

    @Internal
    public int readTargetMethodLine(ClassNode owner, MethodNode methodNode)
    {
        var methodLogName = StringUtils.concatWithSpaces("Method", owner.name + "::" + methodNode.name + methodNode.desc);
        var methodLineAnnotation = asmUtils.annotations().findByType(MutationLine.class, methodNode).first();
        ExceptionsUtils.IfNull(
                methodLineAnnotation,
                IllegalStateException.class,
                "Mutation method", methodLogName, "marked as line-targeted but no line was selected! See", MutationLine.class.getName(), "annotation!"
        );

        var lineParam = asmUtils.annotations().properties()
                .get(methodLineAnnotation, "line", Integer.class)
                .orElseThrow(() -> new IllegalStateException("Mutation method " + methodLogName + " marked as line-targeted, line annotation present but 'line' parameter does not exits!"))
        ;

        return lineParam;
    }

    @Internal
    public MethodMutationPointTarget readTargetCallMethod(ClassNode owner, MethodNode methodNode)
    {
        var ret = new MethodMutationPointTarget();

        var methodTargetAnnotation = asmUtils.annotations()
                .findByType(MutationMethodPoint.class, methodNode)
                .first()
        ;

        var logMethodName = Type.getObjectType(owner.name).getClassName() + "::" + methodNode.name;

        ExceptionsUtils.IfNull(
                methodTargetAnnotation,
                IllegalStateException.class,
                "Mutation method", logMethodName, "has injection point", MutationPoints.MethodCall, "but hasn't", MutationMethodPoint.class.getName(), "annotation!"
        );

        // Computing target callable method owner
        {
            // Trying to load target from 'owner' param
            var targetOwner = asmUtils.annotations().properties()
                    .get(methodTargetAnnotation, "owner", Type.class)
                    .orNull()
            ;

            // If target owner not readed from 'owner' param, that trying to load it from 'ownerName' param
            if (targetOwner == null)
            {
                var className = asmUtils.annotations().properties()
                        .get(methodTargetAnnotation, "ownerName", String.class)
                        .orNull()
                ;

                targetOwner = asmUtils.types().findObjectTypeByName(className);
            }

            if (targetOwner == null)
            {
                logger.warn("Mutation method", logMethodName, "hasn't selected target call-method owner. It produces owner-ignore target search! Are you sure?");
            }

            ret.setOwner(targetOwner);
        }

        // Reading call stage
        {
            var stage = asmUtils.annotations().properties()
                    .get(methodTargetAnnotation, "stage", String.class)
                    .map(MethodCallStage::valueOf)
                    .orElse(MethodCallStage.MutateBefore)
            ;

            ret.setStage(stage);
        }

        // Reading target method name and desc
        {
            // Trying to load method name from annotation name 'property' or using mutation method name
            var name = asmUtils.annotations().properties()
                    .get(methodTargetAnnotation, "name", String.class)
                    .orElseThrow(() -> new IllegalStateException("Mutation method " + logMethodName + " was not select call-method-target name!"))
            ;

            // Trying to load method descriptor from annotation or mutation method params
            var desc = "";

            // Trying to compute method descriptor from Class-array 'paramTypes' annotation param
            {
                var params = asmUtils.annotations().properties()
                        .getList(methodTargetAnnotation, "paramTypes", Type.class)
                        .orNull()
                ;

                if (params != null)
                {
                    var buffer = new DynamicString();
                    buffer.append("(");

                    for (var param : params)
                    {
                        buffer.append(param.getDescriptor());
                    }

                    buffer.append(")");

                    desc = buffer.toString();
                }
            }

            // If method desc was not loaded previously than trying to read it from String 'desc' annotation param
            {
                if (StringUtils.isBlank(desc))
                {
                    var annotationDesc = asmUtils.annotations().properties()
                            .get(methodTargetAnnotation, "desc", String.class)
                            .orNull()
                    ;

                    if (!StringUtils.isBlank(annotationDesc))
                    {
                        desc = annotationDesc;
                    }
                }
            }

            if (StringUtils.isBlank(desc))
            {
                desc = "";
                logger.warn("Mutation method", logMethodName, "hasn't selected target call-method params or descriptor. It produces descriptor-ignore(with any params) target search! Are you sure?");
            }

            ret.setMethod(new Method(name, desc));
        }

        // Reading mutation injection repeat count
        {
            var repeatCount = asmUtils.annotations().properties()
                    .get(methodTargetAnnotation, "repeatCount", Integer.class)
                    .orElse(MutationRepeat.MutateOnce)
            ;

            ret.setRepeatCount(repeatCount);
        }

        return ret;
    }

    public boolean skipParamOnTargetComputing(Type type)
    {
        if (Type.getType(MethodController.class).equals(type))
            return true;

        return false;
    }

    public boolean readHasController(MethodNode methodNode)
    {
        return ExtendedStream.of(Type.getArgumentTypes(methodNode.desc))
                .map(Type::getDescriptor)
                .contains(Type.getDescriptor(MethodController.class))
        ;
    }

    public String readTargetInjectionPoint(AnnotationNode mutateAnnotation)
    {
        return asmUtils.annotations().properties()
                .get(mutateAnnotation, "point", String.class)
                .orElse(MutationPoints.Enter)
        ;
    }

    public FunctionsChain1<Type, Boolean> readTargetTypeFilters(AnnotationNode mutateAnnotation)
    {
        var typesSet = new HashSet<String>();
        var cf = asmUtils.classes().filters();
        var chain = new FunctionsChain1<Type, Boolean>();
        var targetTypes = readTargetTypes(mutateAnnotation);

        for (var target : targetTypes)
        {
            var targetClassNode = asmUtils.classes().findByType(target).orNull();

            if (targetClassNode != null)
            {
                if (cf.mod().hasModifier(Opcodes.ACC_INTERFACE).apply(targetClassNode))
                {
                    chain.add((type) -> {
                        var typeClassNode = asmUtils.classes().findByType(type).orNull();
                        if (typeClassNode != null)
                        {
                            return cf.annotations().contains(target).apply(typeClassNode);
                        }

                        return false;
                    });
                }
                else
                {
                    typesSet.add(target.getInternalName());
                }
            }
        }

        if (!typesSet.isEmpty())
            chain.add((type) -> typesSet.contains(type.getInternalName()));

        return chain;
    }

    public ExtendedList<Type> readTargetTypes(AnnotationNode mutateAnnotation)
    {
        var targets = asmUtils.annotations().properties()
                .getList(mutateAnnotation, "target", Type.class)
                .orElseGet(CollectionsSWL::list)
        ;

        asmUtils.annotations().properties().getList(mutateAnnotation, "targetNames", String.class)
                .stream()
                .flatMap(ExtendedList::stream)
                .map(this::readType)
                .each(targets::add)
        ;

        targets.distinct();

        return targets;
    }

    @Internal
    public Type readType(String name)
    {
        return Type.getObjectType(name.replace(".", "/"));
    }
}
