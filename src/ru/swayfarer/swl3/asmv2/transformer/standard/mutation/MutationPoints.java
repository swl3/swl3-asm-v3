package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

public interface MutationPoints
{
    /** Inject mutation method on target method enter*/
    public String Enter = "Enter";

    /** Inject mutation method on target method exists (return or throw) */
    public String Exit = "Exit";

    /** Inject mutation method on some method invocation in target method body*/
    public String MethodCall = "MethodCall";

    /** Inject mutation method on current line in target method body */
    public String Line = "Line";
}
