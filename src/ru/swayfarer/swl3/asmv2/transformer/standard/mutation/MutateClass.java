package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Mark class as mutations holder <br>
 * Mutation holders contains mutation method, fields and interfaces. <br>
 * <li> If method in mutations holder annotated with {@link MutateMethod} than that method will be read as mutation. </li>
 * <li> If element (such field or method) annotated with {@link TargetOwned} than it will be skip and remapped to target element usage. </li>
 * <li> If mutation holder implements some interfaces than its interfaces will be added to target hierarchy. </li>
 * <li> Any else fields and methods are adding to target by default. </li>
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MutateClass
{
    /**
     * The mutation targets. Mutation will be applied to this classes <br>
     * If interface will be added as target class than all direct (not parent and deeper) implementations will be targets automatically. <br>
     */
    public Class<?>[] target() default {};

    /**
     * Class name version of {@link #target()} <br>
     * Accepts class names, internal names and descriptors
     */
    public String[] targetNames() default {};
}
