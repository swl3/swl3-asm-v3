package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import ru.swayfarer.swl3.markers.Internal;

@Internal
public class MethodControllerHelper
{
    @Internal
    public static void setInvocationInstance(MethodController methodController, Object value)
    {
        methodController.invocationInstance.set(value);
    }

    @Internal
    public static void setReturnValue(MethodController methodController, Object value)
    {
        methodController.resultRef.set(value);
    }

    @Internal
    public static void setException(MethodController methodController, Throwable throwable)
    {
        methodController.exceptionRef.set(throwable);
    }
}
