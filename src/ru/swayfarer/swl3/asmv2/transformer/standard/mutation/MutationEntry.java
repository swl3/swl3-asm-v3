package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;

import java.util.UUID;

@Getter
@Setter
@Accessors(chain = true)
public class MutationEntry<Mutation_Type>
{
    @Internal
    @NonNull
    public UUID trackId = UUID.randomUUID();

    @Internal
    @NonNull
    public IFunction1<Type, Boolean> targetFilters;

    @Internal
    @NonNull
    public ClassNode mutationHolder;

    @Internal
    @NonNull
    public Mutation_Type mutation;
}
