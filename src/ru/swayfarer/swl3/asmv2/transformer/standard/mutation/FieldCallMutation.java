package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.asm.MethodVisitor;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.commons.MethodRemapper;
import ru.swayfarer.swl3.asmv2.asm.commons.Remapper;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class FieldCallMutation
{
    @Internal
    @NonNull
    public MethodNode mutationMethod;

    @Internal
    @NonNull
    public String fieldName;

    @Internal
    @NonNull
    public Type fieldType;

    @Internal
    public Type owner;

    @Internal
    @NonNull
    public String mode;

    @Internal
    @NonNull
    public Type originalOwner;

    public void cloneContentTo(MethodVisitor mv, Type newOwner)
    {
        Remapper remapper = new Remapper()
        {
            @Override
            public String map(String internalName)
            {
                if (internalName.equals(originalOwner.getInternalName()))
                    return newOwner.getInternalName();

                return super.map(internalName);
            }
        };

        mv = new MethodRemapper(mv, remapper);

        mutationMethod.accept(mv);
    }
}
