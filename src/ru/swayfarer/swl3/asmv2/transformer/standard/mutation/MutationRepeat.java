package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

public interface MutationRepeat
{
    /** Inject mutation method at first suitable method call in target method body */
    public int MutateOnce = 1;

    /** Inject mutation method at all suitable methods calls in target method body */
    public int MutateAll = Integer.MIN_VALUE;
}
