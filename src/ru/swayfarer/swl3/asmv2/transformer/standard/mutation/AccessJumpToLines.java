package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Allows jumps to specified lines invoked by {@link MethodController#jumpToLine(int)} method <br>
 * <h3>
 *     Decompiler is a lie! Please, be careful and recheck target lines.
 *     Sometimes the decompiler does not correctly detect line numbers of code!
*  </h3>
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface AccessJumpToLines
{
    public int[] lines();
}
