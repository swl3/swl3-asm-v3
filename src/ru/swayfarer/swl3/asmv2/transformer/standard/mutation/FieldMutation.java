package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.ClassVisitor;
import ru.swayfarer.swl3.asmv2.asm.FieldVisitor;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.commons.FieldRemapper;
import ru.swayfarer.swl3.asmv2.asm.commons.Remapper;
import ru.swayfarer.swl3.asmv2.asm.tree.FieldNode;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class FieldMutation
{
    @Internal
    @NonNull
    public FieldNode originalField;

    @Internal
    @NonNull
    public Type originalOwner;

    public void cloneContentTo(ClassVisitor cv, Type newOwner)
    {
        var fv = cv.visitField(originalField.access, originalField.name, originalField.desc, originalField.signature, originalField.value);
        cloneContentTo(fv, newOwner);
    }

    public void cloneContentTo(FieldVisitor fv, Type newOwner)
    {
        var remapper = new Remapper()
        {
            @Override
            public String mapType(String internalName)
            {
                if (internalName.equals(originalOwner.getInternalName()))
                    return newOwner.getInternalName();

                return super.mapType(internalName);
            }
        };

        originalField.accept(new FieldRemapper(fv, remapper));
    }
}
