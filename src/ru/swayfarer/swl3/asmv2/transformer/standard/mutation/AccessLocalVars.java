package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Allows to access local variables of mutation-target method <br>
 * Can be applied to {@link MutateMethod} annotated methods <br>
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface AccessLocalVars
{
    /** Stack indexes of local variables of target method accessed to read via {@link MethodController#getLocalVar(int)}*/
    public int[] readIndexes() default {};

    /** Stack indexes of local variables of target method accessed to write via {@link MethodController#changeLocalVar(int, Object)}*/
    public int[] writeIndexes() default {};
}
