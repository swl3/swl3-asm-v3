package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.MethodVisitor;
import ru.swayfarer.swl3.asmv2.asm.Opcodes;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class FieldCallMutator extends MethodVisitor implements Opcodes
{
    @NonNull
    @Internal
    public AsmUtilsV2 asmUtils;

    @NonNull
    @Internal
    public ClassMutationsVisitor classMutationsVisitor;

    @NonNull
    @Internal
    public MutationConfig mutationConfig;

    @Internal
    @NonNull
    public ExtendedList<FieldCallMutation> fieldCallMutations;

    public FieldCallMutator(ClassMutationsVisitor classMutationsVisitor, MethodNode methodNode, MethodVisitor mv)
    {
        super(Opcodes.ASM9, mv);
        this.classMutationsVisitor = classMutationsVisitor;
        this.asmUtils = classMutationsVisitor.asmUtils;
        this.mutationConfig = classMutationsVisitor.mutationConfig;
        this.fieldCallMutations = classMutationsVisitor.fieldCallMutations;
    }

    @Override
    public void visitFieldInsn(int opcode, String owner, String name, String descriptor)
    {
        var mode = (opcode == GETSTATIC || opcode == GETFIELD) ? MutateFieldCall.fieldCallGet : MutateFieldCall.fieldCallSet;
        var staticField = opcode == GETSTATIC || opcode == PUTSTATIC;
        var logFieldName = owner + "::" + name + descriptor;

        var currentFieldMutations = fieldCallMutations.exStream()
                .filter((e) -> mode.equals(e.getMode()))
                .filter((e) -> e.getFieldType().getDescriptor().equals(descriptor))
                .filter((e) -> {
                    var targetOwner = getOwnerOrTarget(e);
                    return targetOwner.getInternalName().equals(owner);
                })
                .filter((e) -> e.getFieldName().equals(name))
                .toExList()
        ;

        ExceptionsUtils.If(
                currentFieldMutations.size() > 1,
                IllegalStateException.class,
                "Found more than one field-call mutations candidates for field", logFieldName, ":", currentFieldMutations.map((m) -> m.getMutationMethod().name)
        );

        if (currentFieldMutations.isNotEmpty())
        {
            var mutation = currentFieldMutations.first();
            var methodCreds = cloneOrGetCachedMethodName(mutation);

            var methodInvokeOpcode = staticField ? INVOKESTATIC : INVOKEVIRTUAL;

            visitMethodInsn(
                    methodInvokeOpcode,
                    classMutationsVisitor.classNode.name,
                    methodCreds[0],
                    methodCreds[1],
                    false
            );
        }
        else
        {
            super.visitFieldInsn(opcode, owner, name, descriptor);
        }
    }

    public String[] cloneOrGetCachedMethodName(FieldCallMutation mutation)
    {
        var key = mutation.getMode() + "::" + getOwnerOrTarget(mutation).getInternalName() + "::" + mutation.getFieldName() + mutation.getFieldType().getDescriptor();

        return getMutationMethodsNames().getOrCreate(key, () -> {
            var method = mutation.getMutationMethod();

            var cv = classMutationsVisitor;

            var newMethodName = mutationConfig.getRandomMutationName(
                    "fieldCall",
                    mutation.getOriginalOwner().getClassName(),
                    mutation.getMutationMethod().name
            );

            var fieldTypeDesc = mutation.getFieldType().getDescriptor();
            var methodDesc = mutation.getMode().equals(MutateFieldCall.fieldCallSet) ? ("(" + fieldTypeDesc + ")V") : "()" + fieldTypeDesc;

            var mv = cv.visitMethod(
                    method.access,
                    newMethodName,
                    methodDesc,
                    null,
                    null
            );

            mutation.cloneContentTo(mv, Type.getObjectType(classMutationsVisitor.classNode.name));

            return new String[] {newMethodName, methodDesc};
        });
    }

    private Type getOwnerOrTarget(FieldCallMutation mutation)
    {
        var ret = mutation.getOwner();
        return ret == null ? Type.getObjectType(classMutationsVisitor.classNode.name) : ret;
    }

    public ExtendedMap<String, String[]> getMutationMethodsNames()
    {
        return classMutationsVisitor.getCustomProperties().getOrCreate("fields-call-mutations-methods", ExtendedMap::new);
    }
}
