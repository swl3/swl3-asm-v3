package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Marks class entry as target owned; <br>
 * Its elements calls will be mapped to mutation target elements calls. <br>
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface TargetOwned
{
}
