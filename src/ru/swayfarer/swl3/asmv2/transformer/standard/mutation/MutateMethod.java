package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Marks method as mutation source method <br>
 * <li>If method static than target will static invoke this method directly</li>
 * <li>If method not static than it will be added to target class with generated name and invoke there</li>
 * <li>If method has {@link MethodController} by last param than more functionality access from mutation method</li>
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MutateMethod
{
    /**
     * The target method descriptor. Allows to set target desc directly. <br>
     * If this param not used than target desc will compute based on mutation method params <br>
     */
    public String desc() default "";

    /**
     * The target method name. Allows to set target name directly. <br>
     * If this param not used than mutation method name will be used.
     */
    public String name() default "";

    /**
     * The mutation injection point. <br>
     * Injection points affects the location of mutation method injection in targets <br>
     * See more in {@link MutationPoints}
     */
    public String point() default MutationPoints.Enter;
}
