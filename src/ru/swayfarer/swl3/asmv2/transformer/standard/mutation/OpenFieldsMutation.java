package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class OpenFieldsMutation
{
    @Internal
    public ExtendedList<FieldEntry> fields = new ExtendedList<>();

    @Getter
    @Setter
    @Accessors(chain = true)
    @AllArgsConstructor
    @NoArgsConstructor
    public static class FieldEntry {

        @NonNull
        @Internal
        public String desc;

        @NonNull
        @Internal
        public String name;
    }
}
