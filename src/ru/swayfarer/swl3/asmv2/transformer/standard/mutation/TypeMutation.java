package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.tree.AnnotationNode;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class TypeMutation
{
    @Internal
    @NonNull
    public ExtendedList<AnnotationNode> newAnnotations = new ExtendedList<>();

    @Internal
    @NonNull
    public ExtendedList<Type> newInterfaces = new ExtendedList<>();
}
