package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

/**
 * Mutation method injection stage by call method location. <br>
 * See {@link MutationMethodPoint}, {@link MutationPoints#MethodCall}
 */
public enum MethodCallStage
{
    /** Inject mutation method after target method */
    MutateAfter,

    /** Inject mutation method before target method */
    MutateBefore
}
