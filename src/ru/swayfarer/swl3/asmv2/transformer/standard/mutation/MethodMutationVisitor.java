package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.asm.MethodVisitor;
import ru.swayfarer.swl3.asmv2.asm.Opcodes;
import ru.swayfarer.swl3.asmv2.asm.commons.AdviceAdapter;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class MethodMutationVisitor extends AdviceAdapter implements Opcodes
{
    @Internal
    @NonNull
    public MutationConfig mutationConfig;

    @Internal
    public MethodNode methodNode;

    @Internal
    public ClassNode classNode;

    @Internal
    public ClassMutationsVisitor classMutationsVisitor;

    @Internal
    public AsmUtilsV2 asmUtils;

    public MethodMutationVisitor(
            MutationConfig mutationConfig,
            ClassMutationsVisitor classMutationsVisitor,
            MethodNode methodNode,
            MethodVisitor methodVisitor
    )
    {
//        super(ASM10_EXPERIMENTAL, methodVisitor);
//        super(ASM10_EXPERIMENTAL, methodVisitor, classMutationsVisitor.classNode.name, methodNode);
//        super(methodVisitor, methodNode.access, methodNode.name, methodNode.desc);
        super(ASM9, methodNode, methodVisitor);
        this.mutationConfig = mutationConfig;
        this.methodNode = methodNode;
        this.classNode = classMutationsVisitor.classNode;
        this.classMutationsVisitor = classMutationsVisitor;
        this.asmUtils = classMutationsVisitor.asmUtils;
    }
}
