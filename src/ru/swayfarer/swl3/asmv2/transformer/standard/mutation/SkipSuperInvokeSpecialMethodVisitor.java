package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.MethodVisitor;
import ru.swayfarer.swl3.asmv2.asm.Opcodes;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.collections.CollectionsSWL;

public class SkipSuperInvokeSpecialMethodVisitor extends MethodVisitor implements Opcodes
{
    public int newCount = 0;
    public String superType;

    public SkipSuperInvokeSpecialMethodVisitor(int api, MethodVisitor mv, String superType)
    {
        super(api, mv);
        this.superType = superType;
    }

    @Override
    public void visitInsn(int opcode)
    {
        if (opcode == NEW)
            newCount ++;

        super.visitInsn(opcode);
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface)
    {
        if (opcode == INVOKESPECIAL && superType.equals(owner) && newCount == 0)
        {
            var argumentTypes = Type.getArgumentTypes(descriptor);

            // Pop method args
            for (var argType : CollectionsSWL.list(argumentTypes).reverse())
            {
                visitInsn(argType.getOpcode(argType.getSize() == 1 ? POP : POP2));
            }

            // Pop this
            visitInsn(POP);

            return;
        }

        newCount --;

        super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
    }
}
