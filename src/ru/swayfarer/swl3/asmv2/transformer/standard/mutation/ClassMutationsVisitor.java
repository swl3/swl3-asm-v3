package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.ClassVisitor;
import ru.swayfarer.swl3.asmv2.asm.ClassWriter;
import ru.swayfarer.swl3.asmv2.asm.FieldVisitor;
import ru.swayfarer.swl3.asmv2.asm.MethodVisitor;
import ru.swayfarer.swl3.asmv2.asm.Opcodes;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.tree.FieldNode;
import ru.swayfarer.swl3.asmv2.classnode.NodeClassVisitor;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.asmv2.transformer.IClassWriterReportedClassVisitor;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.markers.Internal;

import java.lang.reflect.Modifier;

@Getter
@Setter
@Accessors(chain = true)
public class ClassMutationsVisitor extends NodeClassVisitor implements IClassWriterReportedClassVisitor
{
    @Internal
    @NonNull
    public ICachedMutations<MethodMutation> constructorMutationsFun = ICachedMutations.of(() ->
        getMutationConfig().findConstructorMutationsFor(getClassType())
    );

    @Internal
    @NonNull
    public ExtendedList<OpenFieldsMutation.FieldEntry> fieldsToOpen = new ExtendedList<>();

    @Internal
    @NonNull
    public ExtendedList<FieldCallMutation> fieldCallMutations = new ExtendedList<>();

    @Internal
    @NonNull
    public MutationConfig mutationConfig;

    @Internal
    @NonNull
    public ExtendedMap<String, Object> customProperties = new ExtendedMap<>();

    @Internal
    public int classWriterActionNeeded = -1;

    public boolean hasAnyMutation;

    public ClassMutationsVisitor(int api, MutationConfig mutationConfig, AsmUtilsV2 asmUtils, ClassNode classNode, ClassVisitor classVisitor)
    {
        super(api, asmUtils, classNode, classVisitor);
        this.mutationConfig = mutationConfig;
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces)
    {
        hasAnyMutation = mutationConfig.hasAnyMutations(getClassType());

        if (!hasAnyMutation)
        {
            super.visit(version, access, name, signature, superName, interfaces);
            return;
        }

        applyClassWriterAction(ClassWriter.COMPUTE_FRAMES);
        appendNewFields();
        appendNewMethods();

        mutationConfig.findFieldCallMutationsFor(Type.getObjectType(name))
                .map(MutationEntry::getMutation)
                .each((e) -> fieldCallMutations.add(e))
        ;

        mutationConfig.findOpenFieldsFor(Type.getObjectType(name))
                .map(MutationEntry::getMutation)
                .nonNull()
                .map(OpenFieldsMutation::getFields)
                .nonNull()
                .flatMap(ExtendedList::stream)
                .each(fieldsToOpen::add)
        ;

        var newInterfaces = new ExtendedList<Type>();

        mutationConfig.findTypeMutationsFor(Type.getObjectType(name))
                .each((entry) -> {
                    var mutation = entry.getMutation();
                    newInterfaces.addAll(mutation.getNewInterfaces());

                    mutation.getNewAnnotations().each((an) -> {
                        var av = visitAnnotation(an.desc, true);
                        an.accept(av);
                    });
                })
        ;

        if (newInterfaces.isNotEmpty())
        {
            var newIntefacesList = newInterfaces.map(Type::getInternalName);

            if (interfaces != null)
                newIntefacesList.addAll(interfaces);

            newIntefacesList.distinct();
            interfaces = newIntefacesList.toArray(String.class);
        }

        cv.visit(version, access, name, signature, superName, interfaces);
    }

    public void appendNewMethods()
    {
        mutationConfig.findNewMethodsFor(getClassType())
                .each((entry) -> {
                    var mutation = entry.getMutation();
                    mutation.cloneContentTo(this, Type.getObjectType(classNode.name));
                })
        ;
    }

    @Override
    public FieldVisitor visitFieldNode(FieldNode fieldNode)
    {
        if (hasAnyMutation)
        {
            var needsToOpen = fieldsToOpen.exStream()
                    .filter((fieldEntry) -> fieldEntry.desc.equals(fieldNode.desc))
                    .filter((fieldEntry) -> fieldEntry.name.equals(fieldNode.name))
                    .findAny().isPresent()
                    ;

            if (needsToOpen)
            {
                if (Modifier.isFinal(fieldNode.access))
                {
                    fieldNode.access = fieldNode.access ^ Opcodes.ACC_FINAL;
                }
            }
        }

        return super.visitFieldNode(fieldNode);
    }

    public void appendNewFields()
    {
        mutationConfig.findNewFieldsFor(getClassType())
                .each((entry) -> {
                    var mutation = entry.getMutation();
                    mutation.cloneContentTo(this, Type.getObjectType(classNode.name));
                })
        ;
    }

    @Override
    public MethodVisitor visitMethodNode(MethodNode methodNode)
    {
        var mv = super.visitMethodNode(methodNode);

        if (hasAnyMutation)
        {
            if (asmUtils.methods().isPrimartyConstructor(getClassType(), methodNode))
            {
                var injectionsList = new ExtendedList<String>();

                constructorMutationsFun.apply()
                        .each((entry) -> {
                            var mutationMethod = entry.getMutation().getOriginalMethod();
                            String newMethodName = cloneConstuctorMutation(entry, mutationMethod);
                            injectionsList.add(newMethodName);
                        })
                ;

                mv = new ConstructorMutator(classNode.name, injectionsList, methodNode, mv);
            }

            if (mutationConfig.findMethodMutationsFor(Type.getObjectType(classNode.name)).findAny().isPresent())
            {
                mv = new DefaultMethodMutator(mutationConfig, this, methodNode, mv);
            }

            if (fieldCallMutations.isNotEmpty())
            {
                mv = new FieldCallMutator(this, methodNode, mv);
            }
        }

        return mv;
    }

    private String cloneConstuctorMutation(MutationEntry<MethodMutation> entry, MethodNode mutationMethod)
    {
        var newMethodName = mutationConfig.getRandomMutationName(
                "init",
                entry.mutationHolder.name,
                entry.mutation.originalMethod.name
        );

        var cmv = visitMethod(
                mutationMethod.access,
                newMethodName,
                mutationMethod.desc,
                mutationMethod.signature,
                null
        );

        cmv = new SkipSuperInvokeSpecialMethodVisitor(Opcodes.ASM_CURRENT, cmv, entry.mutationHolder.superName);

        entry.getMutation().cloneContentTo(cmv, Type.getObjectType(classNode.name));
        return newMethodName;
    }

    public void applyClassWriterAction(int newAction)
    {
        if (classWriterActionNeeded < newAction)
            classWriterActionNeeded = newAction;
    }
}
