package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.var;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.thread.ThreadsUtils;

public interface ICachedMutations<Mutation_Type> extends IFunction0<ExtendedStream<MutationEntry<Mutation_Type>>>
{
    public static <T> ICachedMutations<T> of(IFunction0<ExtendedStream<MutationEntry<T>>> valueFun)
    {
        var fun = ThreadsUtils.singleton(valueFun);
        fun = fun.andApply((stream) -> stream.toExList().exStream());

        return fun::apply;
    }
}
