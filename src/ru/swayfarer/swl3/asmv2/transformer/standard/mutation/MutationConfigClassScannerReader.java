package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.classscan.ClassPathClassesInPackageProvider;
import ru.swayfarer.swl3.asmv2.classscan.ClassScanner;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class MutationConfigClassScannerReader
{
    @Internal
    @NonNull
    public AsmUtilsV2 asmUtilsV2;

    @Internal
    @NonNull
    public MutationConfigReader configReader;

    @Internal
    @NonNull
    public ClassScanner classScanner;

    public MutationConfigClassScannerReader(@NonNull MutationConfigReader configReader)
    {
        this(configReader, new ClassScanner(new ClassPathClassesInPackageProvider(), configReader.getAsmUtils().classes().getClassNodeCache()));
    }

    public MutationConfigClassScannerReader(@NonNull MutationConfigReader configReader, @NonNull ClassScanner classScanner)
    {
        this.asmUtilsV2 = configReader.getAsmUtils();
        this.configReader = configReader;
        this.classScanner = classScanner;
    }

    public void scan(String pkg)
    {
        var cf = asmUtilsV2.classes().filters();
        classScanner.stream(pkg)
                .filter(cf.annotations().contains(MutateClass.class))
                .each((classNode) -> {
                    configReader.readMutations(classNode);
                })
        ;
    }
}
