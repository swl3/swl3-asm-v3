package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.markers.Internal;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Getter
@Setter
@Accessors(chain = true)
public class MethodController
{
    @Internal
    public static Object noReplacedArg = new Object();

    @Internal
    public AtomicReference<Throwable> exceptionRef = new AtomicReference<>();

    @Internal
    public AtomicReference<Object> resultRef = new AtomicReference<>();

    @Internal
    public AtomicBoolean returnTouched = new AtomicBoolean();

    @Internal
    public AtomicReference<Object> invocationInstance = new AtomicReference<>();

    @Internal
    public AtomicInteger lineToJump = new AtomicInteger(-1);

    @Internal
    public ExtendedMap<Integer, Object> newLocalVars = new ExtendedMap<>();

    @Internal
    public ExtendedMap<Integer, Object> readLocalVars = new ExtendedMap<>();

    /**
     * For void methods (for non-void returns null. Be accurate!)
     */
    public void doReturn()
    {
        returnTouched.set(true);
    }

    /**
     * Sets return value of target method <br>
     * If this method invoked than target method will stop and return selected value <br>
     * For <code>void</code> method invokes method break (aka <code>return;</code>)

     * @param obj Target return value
     */
    public void setReturnValue(Object obj)
    {
        returnTouched.set(true);
        resultRef.set(obj);
    }

    /**
     * Get target method return value if injecting on exit <br>
     */
    public Object getReturnValue()
    {
        return resultRef.get();
    }

    /**
     * Get line to target method line jump. Goto instruction analog. <br>
     * <h3>Be accurate with that!</h3>
     */
    public int getLineToJump()
    {
        return lineToJump.get();
    }

    /**
     * Make jump to selected line. <br>
     * For use it you must select target line in {@link AccessJumpToLines#lines()} parameter
     * and annotate mutation method with that annotation.
     * @param line Target line
     */
    public void jumpToLine(int line)
    {
        lineToJump.set(line);
    }

    /**
     * Get instance object which called target method. <br>
     * For static methods will be null
     */
    public Object getInvocationIntance()
    {
        return invocationInstance.get();
    }

    /**
     * Allows to change method local variable value <br>
     * Works only for variables which indexes set in {@link AccessLocalVars#writeIndexes()}
     */
    public void changeLocalVar(int index, Object newValue)
    {
        newLocalVars.put(index, newValue);
    }

    /**
     * Allows to get method local variable value <br>
     * Works only for variables which indexes set in {@link AccessLocalVars#readIndexes()}
     */
    public <T> ExtendedOptional<T> getLocalVar(int index)
    {
        return ExtendedOptional.of((T) readLocalVars.get(index));
    }

    /* -------------------------------------------------------------------------------------------------
     *                                       Internal logic
     * ------------------------------------------------------------------------------------------------- */

    /**
     * @deprecated Be accurate with this method! For internal use only! Dangerous! Evil!!
     */
    @Deprecated
    @Internal
    public MethodController clear()
    {
        invocationInstance.set(null);
        returnTouched.set(false);
        resultRef.set(null);
        exceptionRef.set(null);
        newLocalVars.clear();
        readLocalVars.clear();

        return this;
    }

    /**
     * @deprecated Be accurate with this method! For internal use only! Dangerous! Evil!!
     */
    @Deprecated
    @Internal
    public boolean isLocalVarsChanged()
    {
        return newLocalVars.isEmpty();
    }

    /**
     * @deprecated Be accurate with this method! For internal use only! Dangerous! Evil!!
     */
    @Deprecated
    @Internal
    public boolean isLineJumpApplied()
    {
        return newLocalVars.isEmpty();
    }

    /**
     * @deprecated Be accurate with this method! For internal use only! Dangerous! Evil!!
     */
    @Deprecated
    @Internal
    public boolean isLocalVarChanged(int index)
    {
        var value = getNewVarValue(index);
        return value != null && value != noReplacedArg;
    }

    /**
     * @deprecated Be accurate with this method! For internal use only! Dangerous! Evil!!
     */
    @Deprecated
    @Internal
    public boolean isLocalVarsRead()
    {
        return readLocalVars.isEmpty();
    }

    /**
     * @deprecated Be accurate with this method! For internal use only! Dangerous! Evil!!
     */
    @Deprecated
    @Internal
    public void putLocalVar(int index, Object value)
    {
        readLocalVars.put(index, value);
    }

    /**
     * @deprecated Be accurate with this method! For internal use only! Dangerous! Evil!!
     */
    @Deprecated
    @Internal
    public Object getNewVarValue(int index)
    {
        return newLocalVars.getOrDefault(index, noReplacedArg);
    }

    /**
     * @deprecated Be accurate with this method! For internal use only! Dangerous! Evil!!
     */
    @Deprecated
    @Internal
    public boolean isReturnTouched()
    {
        return returnTouched.get();
    }
}
