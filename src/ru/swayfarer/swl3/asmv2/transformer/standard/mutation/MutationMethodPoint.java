package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Adds custom target information for methods that annotated with {@link MutateMethod} <br>
 * Mutation method needs to be declared with {@link MutationPoints#MethodCall} point. <br>
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MutationMethodPoint
{
    /**
     * Call method owner. If mutation point targeted to java.lang.String.trim() than this param must be set to {@link String}
     */
    public Class<?> owner() default Object.class;

    /**
     * Call method owner name. Name analog of {@link #owner()} <br>
     * Accepts class names, internal names and descriptors <br>
     */
    public String ownerName() default "";

    /**
     * Call method name. If mutation point targeted to java.lang.String.trim() than this param must be set to <code>"name"</code>
     */
    public String name() default "";

    /**
     * Call method parameter types as class array. Empty array marks call method params to compute by mutation method params <br>
     * Can be aliased by {@link #desc()}
     */
    public Class<?>[] paramTypes() default {};

    /**
     * Call method descriptor. Empty marks call method params to compute by mutation method params <br>
     * Can be aliased by {@link #paramTypes()}
     */
    public String desc() default "";

    /**
     * Mutation injection stage on method call.
     */
    public MethodCallStage stage() default MethodCallStage.MutateBefore;

    /**
     * The injection repeat count. <br>
     */
    public int repeatCount() default MutationRepeat.MutateOnce;
}
