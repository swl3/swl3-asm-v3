package ru.swayfarer.swl3.asmv2.transformer.standard.mutation;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.asm.ClassVisitor;
import ru.swayfarer.swl3.asmv2.transformer.VisitorClassNodeTransformer;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class MutationClassTransformer extends VisitorClassNodeTransformer
{
    @Internal
    @NonNull
    public MutationConfig mutationConfig;

    public MutationClassTransformer(MutationConfig mutationConfig)
    {
        this(mutationConfig, AsmUtilsV2.getInstance());
    }

    public MutationClassTransformer(MutationConfig mutationConfig, @NonNull AsmUtilsV2 asmUtils)
    {
        this.asmUtils = asmUtils;
        this.mutationConfig = mutationConfig;
    }

    @Override
    public ClassVisitor createVisitor(ClassNode classNode)
    {
        return null;
    }
}
