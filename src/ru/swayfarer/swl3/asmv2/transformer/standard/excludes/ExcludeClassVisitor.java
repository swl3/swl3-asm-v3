package ru.swayfarer.swl3.asmv2.transformer.standard.excludes;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.ClassVisitor;
import ru.swayfarer.swl3.asmv2.asm.FieldVisitor;
import ru.swayfarer.swl3.asmv2.asm.MethodVisitor;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.commons.ClassRemapper;
import ru.swayfarer.swl3.asmv2.asm.commons.Remapper;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.asm.tree.FieldNode;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.asmv2.classnode.NodeClassVisitor;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class ExcludeClassVisitor extends NodeClassVisitor
{
    @Internal
    @NonNull
    public ExcludesConfig excludesConfig;

    public boolean excluded;

    public ExcludeClassVisitor(int api, ExcludesConfig excludesConfig, AsmUtilsV2 asmUtils, ClassNode classNode, ClassVisitor cv)
    {
        super(
                api,
                asmUtils,
                classNode,
                // Automatically remaps all blocked types calls to stubs
                new ClassRemapper(
                        cv,
                        new ExcludesConfigRemapper(excludesConfig)
                )
        );

        this.excludesConfig = excludesConfig;
    }

    @Override
    public FieldVisitor visitFieldNode(FieldNode fieldNode)
    {
        if (excludesConfig.needsBlockField(fieldNode))
            return null;

        return super.visitFieldNode(fieldNode);
    }

    @Override
    public MethodVisitor visitMethodNode(MethodNode methodNode)
    {
        if (excludesConfig.needsBlockMethod(methodNode))
            return null;

        var mv = super.visitMethodNode(methodNode);
        return mv;
    }

    @Internal
    @Getter
    @Setter
    @Accessors(chain = true)
    public static class ExcludesConfigRemapper extends Remapper{

        @Internal
        @NonNull
        public ExcludesConfig excludesConfig;

        public ExcludesConfigRemapper(@NonNull ExcludesConfig excludesConfig)
        {
            this.excludesConfig = excludesConfig;
        }

        @Override
        public String map(String internalName)
        {
            if (excludesConfig.needsBlockDesc(Type.getObjectType(internalName).getDescriptor()))
                return Type.getType($$StubClass.class).getInternalName();

            return super.map(internalName);
        }
    }
}
