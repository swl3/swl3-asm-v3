package ru.swayfarer.swl3.asmv2.transformer.standard.excludes;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.tree.AnnotationNode;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.asm.tree.FieldNode;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class ExcludesConfig
{
    @Internal
    @NonNull
    public AsmUtilsV2 asmUtils;

    @Internal
    public FunctionsChain1<ExtendedList<AnnotationNode>, Boolean> annotationsExcludesFilter = new FunctionsChain1<>();

    @Internal
    @NonNull
    public FunctionsChain1<ClassNode, Boolean> classExcludesFilter = new FunctionsChain1<>();

    @Internal
    @NonNull
    public FunctionsChain1<MethodNode, Boolean> methodExcludeFilter = new FunctionsChain1<>();

    @Internal
    @NonNull
    public FunctionsChain1<FieldNode, Boolean> fieldExcludesFilter = new FunctionsChain1<>();

    public ExcludesConfig(@NonNull AsmUtilsV2 asmUtils)
    {
        this.asmUtils = asmUtils;
    }

    public boolean needsBlockMethod(MethodNode methodNode)
    {
        var returnType = Type.getReturnType(methodNode.desc);
        var methodParams = Type.getArgumentTypes(methodNode.desc);

        if (needsBlockDesc(returnType.getDescriptor()))
            return true;

        for (var methodParamType : methodParams)
        {
            if (needsBlockDesc(methodParamType.getDescriptor()))
                return true;
        }

        var annotations = CollectionsSWL.list(asmUtils.annotations().findAll(methodNode));

        if (annotations != null)
        {
            if (Boolean.TRUE.equals(this.annotationsExcludesFilter.apply(annotations)))
                return true;
        }

        return Boolean.TRUE.equals(this.methodExcludeFilter.apply(methodNode));
    }

    public boolean needsBlockField(FieldNode fieldNode)
    {
        var fieldType = Type.getType(fieldNode.desc);

        if (needsBlockDesc(fieldType.getDescriptor()))
            return true;

        var annotations = CollectionsSWL.list(asmUtils.annotations().findAll(fieldNode));

        if (annotations != null)
        {
            if (Boolean.TRUE.equals(this.annotationsExcludesFilter.apply(annotations)))
                return true;
        }

        return Boolean.TRUE.equals(this.fieldExcludesFilter.apply(fieldNode));
    }

    public boolean needsBlockDesc(String descriptor)
    {
        var classNode = asmUtils.classes().findByDescriptor(descriptor).orNull();

        if (classNode == null)
            return false;

        var annotations = CollectionsSWL.list(asmUtils.annotations().findAll(classNode));

        if (annotations != null)
        {
            if (Boolean.TRUE.equals(this.annotationsExcludesFilter.apply(annotations)))
                return true;
        }

        return Boolean.TRUE.equals(this.classExcludesFilter.apply(classNode));
    }
}
