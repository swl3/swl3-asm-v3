package ru.swayfarer.swl3.asmv2.transformer;

public interface IClassWriterReportedClassVisitor
{
    public int getClassWriterActionNeeded();
}
