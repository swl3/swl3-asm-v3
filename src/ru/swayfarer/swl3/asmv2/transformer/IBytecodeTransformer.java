package ru.swayfarer.swl3.asmv2.transformer;

public interface IBytecodeTransformer
{
    public void transform(BytecodeEvent bytecodeEvent);
}
