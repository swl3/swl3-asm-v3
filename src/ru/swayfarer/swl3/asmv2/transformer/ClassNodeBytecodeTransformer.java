package ru.swayfarer.swl3.asmv2.transformer;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.asmv2.asm.ClassReader;
import ru.swayfarer.swl3.asmv2.asm.ClassWriter;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class ClassNodeBytecodeTransformer implements IBytecodeTransformer
{
    @NonNull
    @Internal
    public ExtendedList<IClassNodeTransformer> transformers = new ExtendedList<>();

    @Override
    public void transform(BytecodeEvent bytecodeEvent)
    {
        byte[] newBytes = null;

        try
        {
            var classNode = new ClassNode();
            var classReader = new ClassReader(bytecodeEvent.getClassBytes());
            classReader.accept(classNode, ClassReader.EXPAND_FRAMES);

            var event = new ClassNodeTransformEvent()
                    .setClassNode(classNode)
            ;

            for (var transformer : transformers)
            {
                transformer.transform(event);
            }

            if (event.isTransformed())
            {
                bytecodeEvent.setClassWriterAction(event.getClassWriterAction());

                try
                {
                    var classWriter = new ClassWriter(event.getClassWriterAction());
                    classNode.accept(classWriter);
                    newBytes = classWriter.toByteArray();
                }
                catch (Throwable e)
                {
                    try
                    {
                        var classWriter = new ClassWriter(0);
                        classNode.accept(classWriter);
                        newBytes = classWriter.toByteArray();
                    }
                    catch (Throwable e2)
                    {
                        // Nope!
                    }

                    throw e;
                }

                var newName = classNode.name;

                bytecodeEvent
                        .setClassBytes(newBytes)
                        .setName(newName)
                ;
            }
        }
        catch (Throwable e)
        {
            var newClassDumpMessage = "";
            var dumpFile = FileSWL.of("dumps/transformers/error/" + bytecodeEvent.getName().replace(".", "/").concat(".class"));
            if (newBytes != null)
            {
                dumpFile.out().writeBytes(newBytes).close();
                newClassDumpMessage = " New class bytes was dumped to " + dumpFile.getAbsolutePath();
            }

            throw new RuntimeException("Can't transform class " + bytecodeEvent.getName() + "." + newClassDumpMessage, e);
        }
    }

    public ClassNodeBytecodeTransformer addClassNodeTransformer(IClassNodeTransformer transformer)
    {
        if (transformer != null)
            this.transformers.add(transformer);

        return this;
    }
}
