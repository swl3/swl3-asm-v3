package ru.swayfarer.swl3.asmv2.proxy;

public class InvocationHandlers
{
    public static FilteredProxyHandler filteredInvocationHandler()
    {
        return filteredInvocationHandler(true);
    }

    public static FilteredProxyHandler filteredInvocationHandler(boolean superIfNotTouched)
    {
        return new FilteredProxyHandler(superIfNotTouched);
    }

    public static IReflectionProxyHandler appendBefore(IReflectionProxyHandler handler, IReflectionProxyHandler target)
    {
        return (event) -> {
            target.handle(event);
            handler.handle(event);
        };
    }

    public static IReflectionProxyHandler appendAfter(IReflectionProxyHandler handler, IReflectionProxyHandler target)
    {
        return (event) -> {
            handler.handle(event);
            target.handle(event);
        };
    }
}
