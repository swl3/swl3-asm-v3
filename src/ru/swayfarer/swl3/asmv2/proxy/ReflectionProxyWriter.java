package ru.swayfarer.swl3.asmv2.proxy;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.ClassVisitor;
import ru.swayfarer.swl3.asmv2.asm.Handle;
import ru.swayfarer.swl3.asmv2.asm.MethodVisitor;
import ru.swayfarer.swl3.asmv2.asm.Opcodes;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.commons.AdviceAdapter;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.markers.Internal;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Method;
import java.util.UUID;

@Getter
@Setter
@Accessors(chain = true)
public class ReflectionProxyWriter implements Opcodes
{
    @Internal
    @NonNull
    public ReflectionProxyGeneratorSettings proxySettings;

    @Internal
    @NonNull
    public AsmUtilsV2 asmUtils;

    public ReflectionProxyWriter(@NonNull ReflectionProxyGeneratorSettings proxySettings)
    {
        this.proxySettings = proxySettings;
        this.asmUtils = proxySettings.getAsmUtils();
    }

    public void writeProxyClass(ClassVisitor cv, ReflectionEndProxyConfiguration endProxyConfiguration)
    {
        cv.visit(endProxyConfiguration.getVersion(), endProxyConfiguration.getAccess(), endProxyConfiguration.getName(), null, endProxyConfiguration.getParentType().getInternalName(), endProxyConfiguration.getInterfaces().exStream().map(Type::getInternalName).toArray(String.class));

        var handlerFieldAccess = endProxyConfiguration.isHandlerFieldStatic() ? ACC_PUBLIC | ACC_STATIC : ACC_PUBLIC;
        cv.visitField(handlerFieldAccess, proxySettings.getInvocationHandlerFieldName(), proxySettings.getInvocationHandlerType().getDescriptor(), null, null);

        for (var newField : endProxyConfiguration.getNewFields())
        {
            cv.visitField(newField.getMod(), newField.getName(), newField.getType().getDescriptor(), null, null);
        }

        for (var interfaceType : endProxyConfiguration.getInterfaces())
        {
            writeClassInvocations(asmUtils.classes().findByType(interfaceType).orElseThrow(() -> new RuntimeException("Can't find class info for interface " + interfaceType.getClassName() + "!")), cv, endProxyConfiguration);
        }

        var parentType = endProxyConfiguration.getParentType();
        if (parentType != null && !parentType.equals(Type.getType(Object.class)))
        {
            var parentClassNode = asmUtils.classes().findByType(parentType).orElseThrow(() -> new RuntimeException("Can't find parent class node " + parentType.getClassName()));
            writeClassInvocations(parentClassNode, cv, endProxyConfiguration);
            writeConstructorsInvocations(parentClassNode, cv, endProxyConfiguration);
        }
        else
        {
            var cmv = cv.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            cmv.visitCode();
            {
                cmv.visitVarInsn(ALOAD, 0);
                cmv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
                cmv.visitInsn(RETURN);
            }
            cmv.visitMaxs(-1, -1);
            cmv.visitEnd();
        }
    }

    public void writeConstructorsInvocations(ClassNode classNode, ClassVisitor cv, ReflectionEndProxyConfiguration endProxyConfiguration)
    {
        var mf = asmUtils.methods().filters();
        asmUtils.methods().stream(classNode).filter(mf.constructors()).filter(mf.mod().publics().or(mf.mod().protecteds())).each((constructorMethodNode) -> {
            var cmv = cv.visitMethod(constructorMethodNode.access, "<init>", constructorMethodNode.desc, null, null);

            writeMethodMetadata(constructorMethodNode, cmv);

            cmv.visitCode();
            {
                cmv.visitVarInsn(ALOAD, 0);
                var paramTypes = Type.getArgumentTypes(constructorMethodNode.desc);

                var nextLocalStackIndex = 1;
                for (var paramType : paramTypes)
                {
                    cmv.visitVarInsn(paramType.getOpcode(ILOAD), nextLocalStackIndex);
                    nextLocalStackIndex += paramType.getSize();
                }

                cmv.visitMethodInsn(Opcodes.INVOKESPECIAL, classNode.name, "<init>", constructorMethodNode.desc, false);

                cmv.visitInsn(RETURN);
            }
            cmv.visitMaxs(-1, -1);
            cmv.visitEnd();
        });
    }

    public void writeClassInvocations(ClassNode classNode, ClassVisitor cv, ReflectionEndProxyConfiguration endProxyConfiguration)
    {
        var cf = asmUtils.classes().filters();
        var mf = asmUtils.methods().filters();

        asmUtils.methods().stream(classNode).not().filter(mf.mod().statics()).not().filter(mf.mod().finals()).not().filter(mf.constructors()).filter(mf.mod().protecteds().or(mf.mod().publics())).each((methodToOverrideNode) -> {
            var hasSuper = !mf.mod().abstracts().apply(methodToOverrideNode);

            var mv = new AdviceAdapter(methodToOverrideNode, cv.visitMethod(ACC_PUBLIC, methodToOverrideNode.name, methodToOverrideNode.desc, null, null));

            writeMethodMetadata(methodToOverrideNode, mv);

            mv.visitCode();
            {
                var thisType = Type.getObjectType(endProxyConfiguration.getName().replace(".", "/"));
                var invocationEventType = proxySettings.getInvocationEventType();
                var invocationEventVarId = mv.newLocal(invocationEventType);
                var invocationEventLoadArgsMethod = proxySettings.getInvocationEventAddArgMethod();

                var invocationHandlerFieldName = proxySettings.getInvocationHandlerFieldName();
                var invocationHandlerType = proxySettings.getInvocationHandlerType();
                var invocationHandlerHandleMethod = proxySettings.getInvocationHandlerHandleMethod();
                var invocationHandlerVarId = mv.newLocal(invocationHandlerType);

                var lambdaMethodDesc = "([Ljava/lang/Object;)Ljava/lang/Object;";
                var lambdaMethodName = genRandomName("super_invocation_lambda");

                var originalMethodReturnType = Type.getReturnType(methodToOverrideNode.desc);
                var cachedMethodGetterName = genRandomName("cached_method_field_getter");

                mv.newInstance(invocationEventType);
                mv.dup();
                mv.visitMethodInsn(Opcodes.INVOKESPECIAL, invocationEventType.getInternalName(), "<init>", "()V", false);

                mv.dup();
                mv.storeLocal(invocationEventVarId);

                mv.dup();
                mv.loadThis();
                mv.putField(invocationEventType, "instance", Type.getType(Object.class));

                mv.dup();
                mv.invokeStatic(thisType, asmUtils.commons().method(cachedMethodGetterName, Method.class));
                mv.putField(invocationEventType, "overrideMethod", Type.getType(Method.class));

                mv.loadArgArray();
                mv.invokeVirtual(invocationEventType, invocationEventLoadArgsMethod);

                if (hasSuper)
                {
                    mv.loadLocal(invocationEventVarId);

                    mv.visitVarInsn(ALOAD, 0);
                    mv.visitInvokeDynamicInsn("applyUnsafe", "("+ thisType.getDescriptor() +")Lru/swayfarer/swl3/funs/GeneratedFuns$IFunction1;",
                            new Handle(Opcodes.H_INVOKESTATIC, "java/lang/invoke/LambdaMetafactory", "metafactory", "(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/String;Ljava/lang/invoke/MethodType;Ljava/lang/invoke/MethodType;Ljava/lang/invoke/MethodHandle;Ljava/lang/invoke/MethodType;)Ljava/lang/invoke/CallSite;",
                                    false), Type.getType(lambdaMethodDesc), new Handle(Opcodes.H_INVOKESPECIAL, thisType.getInternalName(), lambdaMethodName, lambdaMethodDesc, false), Type.getType(lambdaMethodDesc));


                    mv.visitFieldInsn(PUTFIELD, invocationEventType.getInternalName(), "invokeSuperFun", "Lru/swayfarer/swl3/funs/GeneratedFuns$IFunction1;");
                }

                mv.loadThis();
                mv.visitFieldInsn(endProxyConfiguration.isHandlerFieldStatic() ? GETSTATIC : GETFIELD, thisType.getInternalName(), invocationHandlerFieldName, invocationHandlerType.getDescriptor());
                mv.storeLocal(invocationHandlerVarId);

                mv.loadLocal(invocationHandlerVarId);
                var lblIfHandlerNotNull = mv.newLabel();

                mv.ifNonNull(lblIfHandlerNotNull);
                {
                    var runtimeExceptionType = Type.getType(RuntimeException.class);

                    mv.newInstance(runtimeExceptionType);
                    mv.dup();
                    mv.visitLdcInsn("Error while processing proxy instance: reflection invocation handler is null!");
                    mv.visitMethodInsn(Opcodes.INVOKESPECIAL, runtimeExceptionType.getInternalName(), "<init>", "(Ljava/lang/String;)V", false);
                    mv.visitInsn(Opcodes.ATHROW);
                }

                mv.visitLabel(lblIfHandlerNotNull);

                mv.loadLocal(invocationHandlerVarId);
                mv.loadLocal(invocationEventVarId);
                mv.invokeInterface(invocationHandlerType, invocationHandlerHandleMethod);

                {
                    var reflectMethodType = Type.getType(Method.class);
                    var cachedMethodFieldName = genRandomName("cached_method_field");
                    cv.visitField(ACC_PUBLIC | ACC_STATIC, cachedMethodFieldName, reflectMethodType.getDescriptor(), null, null);

                    var cmmv = asmUtils.methods().visitAdviceAdapter(cv, ACC_PUBLIC | ACC_STATIC, cachedMethodGetterName, "()" + reflectMethodType.getDescriptor());

                    cmmv.visitCode();
                    {
                        var lblIfNonNull = cmmv.newLabel();
                        var methodLocalVarId = cmmv.newLocal(reflectMethodType);

                        cmmv.getStatic(thisType, cachedMethodFieldName, reflectMethodType);
                        cmmv.storeLocal(methodLocalVarId);

                        cmmv.loadLocal(methodLocalVarId);
                        cmmv.ifNonNull(lblIfNonNull);

                        {
                            cmmv.visitLdcInsn(Type.getObjectType(classNode.name));

                            var reflectClassType = Type.getType(Class.class);
                            var reflectGetDeclaredMethod = asmUtils.commons().method("getDeclaredMethod", Method.class, String.class, Class[].class);

                            var reflectAccessibleObjectType = Type.getType(AccessibleObject.class);
                            var reflectSetAccessibleMethod = asmUtils.commons().method("setAccessible", void.class, boolean.class);

                            cmmv.visitLdcInsn(methodToOverrideNode.name);

                            var originalMethodParams = Type.getArgumentTypes(methodToOverrideNode.desc);

                            cmmv.visitLdcInsn(originalMethodParams.length);
                            cmmv.visitTypeInsn(Opcodes.ANEWARRAY, Type.getInternalName(Class.class));

                            int nextParamId = 0;
                            for (var paramType : originalMethodParams)
                            {
                                cmmv.visitInsn(DUP);
                                cmmv.visitLdcInsn(nextParamId);
                                cmmv.visitLdcInsn(paramType);
                                cmmv.visitInsn(AASTORE);

                                nextParamId++;
                            }

                            cmmv.invokeVirtual(reflectClassType, reflectGetDeclaredMethod);
                            cmmv.storeLocal(methodLocalVarId);

                            cmmv.loadLocal(methodLocalVarId);
                            cmmv.visitInsn(Opcodes.ICONST_1);
                            cmmv.invokeVirtual(reflectAccessibleObjectType, reflectSetAccessibleMethod);

                            cmmv.loadLocal(methodLocalVarId);
                            cmmv.putStatic(thisType, cachedMethodFieldName, reflectMethodType);
                        }

                        cmmv.visitLabel(lblIfNonNull);

                        cmmv.loadLocal(methodLocalVarId);
                        cmmv.returnValue();
                    }
                    cmmv.visitMaxs(-1, -1);
                    cmmv.visitEnd();
                }

                if (hasSuper)
                {
                    // Generate lambda
                    {
                        var amv = asmUtils.methods().visitAdviceAdapter(cv, ACC_PRIVATE | ACC_SYNTHETIC, lambdaMethodName, lambdaMethodDesc);

                        amv.visitCode();
                        {
                            amv.loadThis();
                            var paramsTypes = Type.getArgumentTypes(methodToOverrideNode.desc);
                            var paramsCount = paramsTypes.length;

                            if (paramsCount > 0)
                            {
                                for (var paramNumber = 0; paramNumber < paramsCount; paramNumber++)
                                {
                                    amv.loadArg(0);
                                    amv.visitIntInsn(SIPUSH, paramNumber);
                                    amv.visitInsn(AALOAD);

                                    asmUtils.types().conversions().convert(amv, Type.getType(Object.class), paramsTypes[paramNumber]);
                                }
                            }

                            // Invoke super method
                            amv.visitMethodInsn(Opcodes.INVOKESPECIAL, classNode.name, methodToOverrideNode.name, methodToOverrideNode.desc,

                                    // If class node is an interface (overrides "default" method)
                                    cf.mod().hasModifier(ACC_INTERFACE).apply(classNode));

                            if (originalMethodReturnType.equals(Type.VOID_TYPE))
                            {
                                amv.visitInsn(Opcodes.ACONST_NULL);
                            }
                            else
                            {
                                asmUtils.types().conversions().convert(amv, originalMethodReturnType, Type.getType(Object.class));
                            }

                            amv.visitInsn(Opcodes.ARETURN);
                        }
                        amv.visitMaxs(-1, -1);
                        amv.visitEnd();
                    }
                }

                if (!originalMethodReturnType.equals(Type.VOID_TYPE))
                {
                    mv.loadLocal(invocationEventVarId);
                    mv.invokeVirtual(invocationEventType, asmUtils.commons().method("getResultReturnValue", Object.class));

                    asmUtils.types().conversions().convert(mv, Type.getType(Object.class), originalMethodReturnType);
                }

                mv.returnValue();
            }
            mv.visitMaxs(-1, -1);
            mv.visitEnd();
        });
    }

    private void writeMethodMetadata(MethodNode methodToOverrideNode, MethodVisitor mv)
    {
        if (proxySettings.isKeepMethodAnnotations())
        {
            writeOriginalAnnotations(mv, methodToOverrideNode.visibleAnnotations, true);
            writeOriginalAnnotations(mv, methodToOverrideNode.invisibleAnnotations, false);
        }

        if (proxySettings.isKeepParameterAnnotations())
        {
            writeOriginalParametersAnnotations(mv, methodToOverrideNode);
        }

        writeOriginalParameters(mv, methodToOverrideNode);
    }

    public void writeOriginalParametersAnnotations(MethodVisitor mv, MethodNode methodToOverrideNode)
    {
        writeParameterAnnotations(mv, true, methodToOverrideNode.visibleParameterAnnotations);
        writeParameterAnnotations(mv, false, methodToOverrideNode.invisibleParameterAnnotations);
    }

    private void writeParameterAnnotations(MethodVisitor mv, boolean visible, java.util.List<ru.swayfarer.swl3.asmv2.asm.tree.AnnotationNode>[] parameterAnnotations)
    {
        if (parameterAnnotations != null)
        {
            for (var paramIndex = 0; paramIndex < parameterAnnotations.length; paramIndex++)
            {
                if (parameterAnnotations[paramIndex] != null)
                {
                    for (var annotation : parameterAnnotations[paramIndex])
                    {
                        var av = mv.visitParameterAnnotation(paramIndex, annotation.desc, visible);
                        annotation.accept(av);
                    }
                }
            }
        }
    }

    public void writeOriginalParameters(MethodVisitor mv, MethodNode methodToOverrideNode)
    {
        if (methodToOverrideNode.parameters != null)
        {
            for (var param : methodToOverrideNode.parameters)
            {
                mv.visitParameter(param.name, param.access);
            }
        }
    }

    public void writeOriginalAnnotations(MethodVisitor mv, java.util.List<ru.swayfarer.swl3.asmv2.asm.tree.AnnotationNode> annotations, boolean visible)
    {
        if (!CollectionsSWL.isNullOrEmpty(annotations))
        {
            for (var annotation : annotations)
            {
                var av = mv.visitAnnotation(annotation.desc, visible);

                if (av != null)
                {
                    annotation.accept(av);
                }
            }
        }
    }

    public String genRandomName(String name)
    {
        return "__$swl3_" + name + "_" + UUID.randomUUID().toString().replace("-", "");
    }
}
