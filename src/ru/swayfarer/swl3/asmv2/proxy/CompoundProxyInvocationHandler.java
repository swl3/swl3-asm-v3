package ru.swayfarer.swl3.asmv2.proxy;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class CompoundProxyInvocationHandler implements IReflectionProxyHandler
{
    @Internal
    @NonNull
    public AsmUtilsV2 asmUtilsV2;

    @Internal
    @NonNull
    public ExtendedMap<Class<?>, IFunction0<Object>> methodKeyToInstanceFun = new ExtendedMap<>();

    public CompoundProxyInvocationHandler(@NonNull AsmUtilsV2 asmUtilsV2)
    {
        this.asmUtilsV2 = asmUtilsV2;
    }

    @SneakyThrows
    @Override
    public void handle(ProxyInvocationEvent event)
    {
        var method = event.getOverrideMethod();
        var declaringClass = method.getDeclaringClass();
        var instanceFun = (IFunction0<?>) methodKeyToInstanceFun.get(declaringClass);

        ExceptionsUtils.IfNull(
                instanceFun,
                IllegalStateException.class,
                "Can't invoke compound proxy method because no one compatible instance found!", method
        );

        event.setReturnObj(method.invoke(instanceFun.apply(), event.getArgs().toArray()));
    }

    public CompoundProxyInvocationHandler registerPart(Class<?> classOfPart, IFunction0<Object> valueFun)
    {
        methodKeyToInstanceFun.put(classOfPart, valueFun.memorized());
        return this;
    }
}
