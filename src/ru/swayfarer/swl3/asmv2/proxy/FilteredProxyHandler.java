package ru.swayfarer.swl3.asmv2.proxy;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.markers.Internal;

import java.lang.reflect.Method;

@Getter
@Setter
@Accessors(chain = true)
public class FilteredProxyHandler implements IReflectionProxyHandler
{
    @Internal
    @NonNull
    public FunctionsChain1<ProxyInvocationEvent, Boolean> handlers = new FunctionsChain1<ProxyInvocationEvent, Boolean>()
            .filter(Boolean.TRUE::equals)
    ;

    @Internal
    @NonNull
    public boolean superIfNotTouched;

    public FilteredProxyHandler(boolean superIfNotTouched)
    {
        handlers.exceptionsHandler.configure().throwAny();
        this.superIfNotTouched = superIfNotTouched;
    }

    @Override
    public void handle(ProxyInvocationEvent event)
    {
        var result = handlers.apply(event);

        if (result == null)
            event.invokeSuper(event.getArgs());
    }

    public FilteredProxyHandler addHandle(
            IFunction1<Method, Boolean> filter,
            IFunction1NoR<ProxyInvocationEvent> handlerFun
    )
    {
        handlers.add((event) -> {
            if (handlerFun != null && (filter == null || Boolean.TRUE.equals(filter.apply(event.getOverrideMethod()))))
            {
                handlerFun.apply(event);
                return true;
            }

            return false;
        });

        return this;
    }
}
