package ru.swayfarer.swl3.asmv2.proxy;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Getter
@Setter
@Accessors(chain = true)
public class ReflectionProxyHelper
{
    public static boolean isDumpsEnabled = false;

    @NonNull
    @Internal
    public ReflectionProxyGeneratorSettings generatorSettings;

    @NonNull
    @Internal
    public ReflectionsUtils reflectionsUtils;

    @NonNull
    @Internal
    public AsmUtilsV2 asmUtils;

    @NonNull
    public ReflectionProxyWriter proxyWriter;

    public ReflectionProxyHelper(
            @NonNull AsmUtilsV2 asmUtils,
            @NonNull ReflectionsUtils reflectionsUtils
    )
    {
        this(new ReflectionProxyGeneratorSettings(asmUtils), reflectionsUtils);
    }

    public ReflectionProxyHelper(
            @NonNull ReflectionProxyGeneratorSettings generatorSettings,
            @NonNull ReflectionsUtils reflectionsUtils
    )
    {
        this.generatorSettings = generatorSettings;
        this.proxyWriter = new ReflectionProxyWriter(generatorSettings);
        this.reflectionsUtils = reflectionsUtils;
        this.asmUtils = generatorSettings.getAsmUtils();
    }

    public Class<?> newProxyClass(IReflectionProxyHandler handler, IFunction1NoR<ReflectionEndProxyConfiguration> configurator)
    {
        var configuration = new ReflectionEndProxyConfiguration();
        configurator.apply(configuration);
        var classNode = new ClassNode();
        proxyWriter.writeProxyClass(classNode, configuration);

        var classBytes = asmUtils.classes().getClassBytes(classNode);


        if (isDumpsEnabled)
            FileSWL.of(configuration.getParentType().getClassName() + "__" + configuration.getName() +".class").out().writeBytes(classBytes).close();

        var retClass = reflectionsUtils.types().loadOrDefine(configuration.getName(), classBytes);

        if (configuration.isHandlerFieldStatic())
            reflectionsUtils.fields().setValue(retClass, handler, generatorSettings.getInvocationHandlerFieldName());

        return retClass;
    }

    public <T> T newProxyInstance(IReflectionProxyHandler handler, IFunction1NoR<ReflectionEndProxyConfiguration> configurator, Object... args)
    {
        var proxyClass = newProxyClass(handler, configurator);
        var proxyInstance = reflectionsUtils.constructors().newInstance(proxyClass, args).orThrow();

        reflectionsUtils.fields().setValue(proxyInstance, handler, generatorSettings.getInvocationHandlerFieldName());

        return (T) proxyInstance;
    }
}
