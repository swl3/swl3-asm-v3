package ru.swayfarer.swl3.asmv2.proxy;

import lombok.SneakyThrows;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;

import java.util.Random;

public class Sample
{
    @SneakyThrows
    public String helloWorld()
    {
        var a = 12345;

        if (a == 567)
            System.out.println(123);

        return "12345";
    }
}
