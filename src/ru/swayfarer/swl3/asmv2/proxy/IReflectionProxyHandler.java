package ru.swayfarer.swl3.asmv2.proxy;

public interface IReflectionProxyHandler
{
    public void handle(ProxyInvocationEvent event) throws Throwable;
}
