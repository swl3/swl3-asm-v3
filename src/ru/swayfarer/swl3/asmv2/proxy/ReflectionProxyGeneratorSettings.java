package ru.swayfarer.swl3.asmv2.proxy;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.commons.Method;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.markers.Internal;

import java.util.UUID;

/**
 * The info about proxy generation <br>
 * Configures proxy writers <br>
 * <h3>Not an end-proxy configuration! </h3>
 */
@Getter
@Setter
@Accessors(chain = true)
public class ReflectionProxyGeneratorSettings
{
    @Internal
    @NonNull
    public String invocationHandlerFieldName = "__$swl3__proxy__invocationHandler" + UUID.randomUUID().toString().replace("-", "");

    @Internal
    public boolean keepMethodAnnotations = true;

    @Internal
    public boolean keepParameterAnnotations = true;

    @Internal
    @NonNull
    public Type invocationHandlerType = Type.getType(IReflectionProxyHandler.class);

    @Internal
    @NonNull
    public Type invocationEventType = Type.getType(ProxyInvocationEvent.class);

    @Internal
    @NonNull
    public Method invocationEventAddArgMethod = new Method("loadArgs", "([Ljava/lang/Object;)V");

    @Internal
    @NonNull
    public Method invocationHandlerHandleMethod = new Method("handle", "(" + Type.getDescriptor(ProxyInvocationEvent.class) + ")V");

    @Internal
    @NonNull
    public AsmUtilsV2 asmUtils;

    public ReflectionProxyGeneratorSettings(@NonNull AsmUtilsV2 asmUtils)
    {
        this.asmUtils = asmUtils;
    }
}
