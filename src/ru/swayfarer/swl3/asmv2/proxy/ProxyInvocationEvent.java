package ru.swayfarer.swl3.asmv2.proxy;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

@Getter
@Setter
@Accessors(chain = true)
public class ProxyInvocationEvent
{
    public static Object defaultReturnValue = new Object();

    @NonNull
    @Internal
    public Method overrideMethod;

    @Internal
    public Object instance;

    @NonNull
    @Internal
    public ExtendedList<Object> args = new ExtendedList<>();

    @Internal
    public Object returnObj = defaultReturnValue;

    @NonNull
    @Internal
    public IFunction1<Object[], Object> invokeSuperFun = (args) -> null;

    @SneakyThrows
    public Object invokeSuper(Object... args)
    {
        try
        {
            return invokeSuperFun.apply(args);
        }
        catch (AbstractMethodError e)
        {
            var method = invokeSuperFun.getClass().getMethod("applyUnsafe", Object[].class);
            method.setAccessible(true);
            var params = new Object[1];
            params[0] = (Object) getArgs().toArray();
            return method.invoke(invokeSuperFun, params);
        }
    }

    public ProxyInvocationEvent setReturnValue(Object returnValue)
    {
        this.returnObj = returnValue;
        return this;
    }

    public Object getResultReturnValue()
    {
        if (returnObj == defaultReturnValue)
        {
            DefaultValuesUsageLogger.log(overrideMethod, instance.getClass());

            var methodReturnType = overrideMethod.getReturnType();
            if (methodReturnType.isPrimitive())
            {
                if (methodReturnType.equals(char.class) || ReflectionsUtils.getInstance().types().isNumber(methodReturnType))
                    return 0;

                if (methodReturnType.equals(boolean.class))
                    return false;
            }

            if (methodReturnType.equals(Optional.class))
                return Optional.empty();

            if (methodReturnType.equals(ExtendedOptional.class))
                return ExtendedOptional.empty();

            return null;
        }

        return returnObj;
    }

    @Internal
    public void loadArgs(Object[] obj)
    {
        this.args.setAll(obj);
    }

    @Internal
    public Object getArg(int paramNumber)
    {
        return this.args.get(paramNumber);
    }

    public void invokeSuperOrDefault()
    {
        setReturnObj(invokeSuper(getArgs()));
        setReturnObj(getResultReturnValue());
    }

    @Internal
    public static class DefaultValuesUsageLogger {

        @Internal
        public static AtomicBoolean enabled = new AtomicBoolean(true);

        @Internal
        @NonNull
        public static ExtendedMap<String, Boolean> alreadyLogged = new ExtendedMap<>().concurrent();

        @Internal
        @NonNull
        public static ILogger logger = LogFactory.getLogger();

        public static void log(Method method, Class<?> proxyClass)
        {
            if (enabled.get())
            {
                var key = Type.getType(proxyClass).getInternalName() + "::" + method.getName() + Type.getMethodDescriptor(method);
                alreadyLogged.getOrCreate(key, () -> {
                    logger.warn("Method '", method, "' was not handled by proxy class", proxyClass, ". Using default value. Are you sure?");
                    return true;
                });
            }
        }

        public static void disable()
        {
            enabled.set(false);
        }

        public static void enable()
        {
            enabled.set(true);
        }
    }
}
