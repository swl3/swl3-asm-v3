package ru.swayfarer.swl3.asmv2.proxy;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;
import ru.swayfarer.swl3.asmv2.asm.Opcodes;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.markers.Internal;

import java.util.UUID;

@Getter
@Setter
@Accessors(chain = true)
public class ReflectionEndProxyConfiguration
{
    @NonNull
    @Internal
    public Type parentType = Type.getType(Object.class);

    @NonNull
    @Internal
    public ExtendedList<Type> interfaces = new ExtendedList<>();

    @NonNull
    @Internal
    public ExtendedList<FieldEntry> newFields = new ExtendedList<>();

    public int access = Opcodes.ACC_PUBLIC;

    public int version = Opcodes.V1_8;

    public boolean handlerFieldStatic;

    public String name = "__$SWL3_ASM_V3_PROXY__" + UUID.randomUUID().toString().replace("-", "");

    @Tolerate
    public ReflectionEndProxyConfiguration setParentType(Class<?> classType)
    {
        return this.setParentType(Type.getType(classType));
    }

    public ReflectionEndProxyConfiguration newField(int modifier, String name, Class<?> type)
    {
        this.newFields.add(FieldEntry.builder()
            .name(name)
            .type(Type.getType(type))
            .mod(modifier)
            .build()
        );

        return this;
    }

    @Tolerate
    public ReflectionEndProxyConfiguration setIntefaces(ExtendedList<Class<?>> interfaces)
    {
        return this.setInterfaces(interfaces.map(Type::getType));
    }

    public ReflectionEndProxyConfiguration addInterface(Class<?> interfaceClass)
    {
        getInterfaces().add(Type.getType(interfaceClass));
        return this;
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @SuperBuilder
    public static class FieldEntry {
        @NonNull
        public String name;

        @NonNull
        public Type type;

        public int mod;
    }
}
