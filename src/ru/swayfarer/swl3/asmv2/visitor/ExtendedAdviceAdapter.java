package ru.swayfarer.swl3.asmv2.visitor;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.MethodVisitor;
import ru.swayfarer.swl3.asmv2.asm.Opcodes;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.commons.AdviceAdapter;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;

@Getter
@Setter
@Accessors(chain = true)
public class ExtendedAdviceAdapter extends AdviceAdapter implements Opcodes
{
    public ExtendedAdviceAdapter(int api, MethodVisitor methodVisitor, int access, String name, String descriptor)
    {
        super(api, methodVisitor, access, name, descriptor);
    }

    public ExtendedAdviceAdapter(MethodNode methodNode)
    {
        super(methodNode);
    }

    public ExtendedAdviceAdapter(MethodNode methodNode, MethodVisitor methodVisitor)
    {
        super(methodNode, methodVisitor);
    }

    public ExtendedAdviceAdapter(int api, MethodNode methodNode, MethodVisitor methodVisitor)
    {
        super(api, methodNode, methodVisitor);
    }

    @Override
    public void visitLdcInsn(Object value)
    {
        if (value instanceof Type)
        {
            var type = (Type) value;

            if (type.equals(Type.BYTE_TYPE))
            {
                visitFieldInsn(GETSTATIC, "java/lang/Byte", "TYPE", "Ljava/lang/Class;");
                return;
            }

            if (type.equals(Type.SHORT_TYPE))
                {
                visitFieldInsn(GETSTATIC, "java/lang/Short", "TYPE", "Ljava/lang/Class;");
                return;
            }

            if (type.equals(Type.INT_TYPE))
            {
                visitFieldInsn(GETSTATIC, "java/lang/Integer", "TYPE", "Ljava/lang/Class;");
                return;
            }

            if (type.equals(Type.LONG_TYPE))
            {
                visitFieldInsn(GETSTATIC, "java/lang/Long", "TYPE", "Ljava/lang/Class;");
                return;
            }

            if (type.equals(Type.FLOAT_TYPE))
            {
                visitFieldInsn(GETSTATIC, "java/lang/Float", "TYPE", "Ljava/lang/Class;");
                return;
            }

            if (type.equals(Type.DOUBLE_TYPE))
            {
                visitFieldInsn(GETSTATIC, "java/lang/Double", "TYPE", "Ljava/lang/Class;");
                return;
            }

            if (type.equals(Type.CHAR_TYPE))
            {
                visitFieldInsn(GETSTATIC, "java/lang/Character", "TYPE", "Ljava/lang/Class;");
                return;
            }

            if (type.equals(Type.BOOLEAN_TYPE))
            {
                visitFieldInsn(GETSTATIC, "java/lang/Boolean", "TYPE", "Ljava/lang/Class;");
                return;
            }
        }

        super.visitLdcInsn(value);
    }
}
