package ru.swayfarer.swl3.asmv2.classscan;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.system.SystemUtils;

@Getter
@Setter
@Accessors(chain = true)
public class ClassPathClassesInPackageProvider extends UrlsClassesInPackageProvider
{
    @NonNull
    @Internal
    public SystemUtils systemUtils;

    public ClassPathClassesInPackageProvider()
    {
        this(SystemUtils.getInstance());
    }

    public ClassPathClassesInPackageProvider(SystemUtils systemUtils)
    {
        this.systemUtils = systemUtils;
    }

    @Override
    public void loadClasses()
    {
        var classPathUrls = systemUtils.getClasspathAndClassloaderURLs();
        for (var classPathUrl : classPathUrls)
            addURL(classPathUrl);

        super.loadClasses();
    }
}
