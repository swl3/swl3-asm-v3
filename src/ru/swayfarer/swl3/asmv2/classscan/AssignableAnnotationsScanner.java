package ru.swayfarer.swl3.asmv2.classscan;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.tree.AnnotationNode;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.asm.tree.FieldNode;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class AssignableAnnotationsScanner
{
    @Internal
    @NonNull
    public AsmUtilsV2 asmUtilsV2;

    public AssignableAnnotationsScanner()
    {
        this(AsmUtilsV2.getInstance());
    }


    public AssignableAnnotationsScanner(@NonNull AsmUtilsV2 asmUtilsV2)
    {
        this.asmUtilsV2 = asmUtilsV2;
    }

    public ExtendedList<AnnotationNode> findAnnotations(ClassNode classNode)
    {
        var result = new ExtendedList<AnnotationNode>();
        var alreadyVisited = new ExtendedList<Type>();

        while (classNode != null)
        {
            var annotations = asmUtilsV2.annotations().findAll(classNode);
            mergeAnnotations(annotations, result, alreadyVisited);

            classNode = asmUtilsV2.classes().findByInternalName(classNode.superName).orNull();
        }

        return result;
    }

    public ExtendedList<AnnotationNode> findAnnotations(MethodNode methodNode)
    {
        var result = new ExtendedList<AnnotationNode>();
        var alreadyVisited = new ExtendedList<Type>();

        var annotations = asmUtilsV2.annotations().findAll(methodNode);
        mergeAnnotations(annotations, result, alreadyVisited);

        return result;
    }

    public ExtendedList<AnnotationNode> findAnnotations(FieldNode node)
    {
        var result = new ExtendedList<AnnotationNode>();
        var alreadyVisited = new ExtendedList<Type>();

        var annotations = asmUtilsV2.annotations().findAll(node);
        mergeAnnotations(annotations, result, alreadyVisited);

        return result;
    }

    public void mergeAnnotations(
        ExtendedList<AnnotationNode> targets,
        ExtendedList<AnnotationNode> result,
        ExtendedList<Type> visitedAnnotations
    )
    {
        for (var target : targets)
        {
            var type = Type.getType(target.desc);

            if (!visitedAnnotations.contains(type))
            {
                visitedAnnotations.add(type);
                result.add(target);

                var annotationTypeClassNode = asmUtilsV2.classes().findByType(type);

                annotationTypeClassNode.ifPresent((classNode) ->
                    mergeAnnotations(
                            asmUtilsV2.annotations().findAll(classNode),
                            result,
                            visitedAnnotations
                    )
                );
            }
        }
    }
}
