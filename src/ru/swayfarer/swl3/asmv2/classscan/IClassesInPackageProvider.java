package ru.swayfarer.swl3.asmv2.classscan;

import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

public interface IClassesInPackageProvider extends IFunction1<String, ExtendedStream<ClassToScanEntry>>
{
    @Override
    default ExtendedStream<ClassToScanEntry> applyUnsafe(String s) throws Throwable
    {
        return findClasses(s);
    }

    public ExtendedStream<ClassToScanEntry> findClasses(String packageName) throws Throwable;
}
