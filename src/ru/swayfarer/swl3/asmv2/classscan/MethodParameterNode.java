package ru.swayfarer.swl3.asmv2.classscan;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.asm.tree.AnnotationNode;
import ru.swayfarer.swl3.asmv2.asm.tree.LocalVariableNode;
import ru.swayfarer.swl3.asmv2.asm.tree.ParameterNode;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class MethodParameterNode
{
    @NonNull
    @Internal
    public ParameterNode parameter;

    @NonNull
    @Internal
    public LocalVariableNode variable;

    @NonNull
    @Internal
    public ExtendedList<AnnotationNode> visibleAnnotations = new ExtendedList<>();

    @NonNull
    @Internal
    public ExtendedList<AnnotationNode> invisibleAnnotations = new ExtendedList<>();

    @Internal
    public int parameterNumber;

    public MethodParameterNode()
    {

    }
}
