package ru.swayfarer.swl3.asmv2.classscan;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.classnode.ClassNodeCache;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class ClassScanner
{
    @NonNull
    @Internal
    public ClassNodeCache classNodeCache;

    @NonNull
    @Internal
    public IClassesInPackageProvider packageToClassesFun;

    @NonNull
    @Internal
    public FunctionsChain1<ClassNode, Boolean> classNodeFilters = new FunctionsChain1<ClassNode, Boolean>()
            .filter(Boolean.FALSE::equals)
    ;

    public ClassScanner()
    {
        this(new ClassPathClassesInPackageProvider());
    }

    public ClassScanner(IClassesInPackageProvider packageToClassesFun)
    {
        this(packageToClassesFun, new ClassNodeCache());
    }

    public ClassScanner(IClassesInPackageProvider packageToClassesFun, ClassNodeCache classNodeCache)
    {
        this.packageToClassesFun = packageToClassesFun;
        this.classNodeCache = classNodeCache;
    }

    public ExtendedStream<ClassNode> stream(String packageName)
    {
        if (packageName == null)
            return ExtendedStream.empty();

        return packageToClassesFun.apply(packageName)
                .filter((e) -> e.getName().startsWith(packageName))
                .distinctBy(ClassToScanEntry::getName)
                .map(this::readClassNode)
                .filter((cn) -> !Boolean.FALSE.equals(classNodeFilters.apply(cn)))
        ;
    }

    @Internal
    public ClassNode readClassNode(ClassToScanEntry entry)
    {
        var className = entry.getName();
        var classNode = classNodeCache.getOrCreateClassNode(className, entry.getBytes());
        return classNode;
    }
}
