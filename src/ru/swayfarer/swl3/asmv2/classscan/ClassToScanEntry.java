package ru.swayfarer.swl3.asmv2.classscan;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.streams.StreamsUtils;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.thread.ThreadsUtils;

import java.io.InputStream;

@Getter
@Setter
@Accessors(chain = true)
public class ClassToScanEntry
{
    @Internal
    @NonNull
    public IFunction0<InputStream> inputStreamFun;

    public IFunction0<byte[]> bytesFun = ThreadsUtils.singleton(() -> StreamsUtils.readAll(inputStreamFun.apply()));

    public String name;

    public byte[] getBytes()
    {
        return bytesFun.apply();
    }
}
