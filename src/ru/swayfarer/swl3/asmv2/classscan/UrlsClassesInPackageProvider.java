package ru.swayfarer.swl3.asmv2.classscan;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.exception.LibTags;
import ru.swayfarer.swl3.exception.LibTags.Exceptions;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.streams.StreamsUtils;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.thread.ThreadsUtils;

import java.io.File;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.zip.ZipFile;

@Getter
@Setter
@Accessors(chain = true)
public class UrlsClassesInPackageProvider implements IClassesInPackageProvider
{
    @Internal
    public boolean skipJavaHome = true;

    @Internal
    @NonNull
    public ExtendedList<ClassToScanEntry> classEntries = new ExtendedList<>();

    @Internal
    @NonNull
    public ExtendedList<URL> urls = new ExtendedList<>();

    @Internal
    public IFunction1NoR<IFunction0NoR> onceRunner = ThreadsUtils.once();

    @Internal
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();

    @SneakyThrows
    public UrlsClassesInPackageProvider addFile(File file)
    {
        urls.add(file.toURI().toURL());
        return this;
    }

    public UrlsClassesInPackageProvider addURL(URL url)
    {
        urls.add(url);
        return this;
    }

    @SneakyThrows
    public void loadClasses()
    {
        var javaHome = System.getProperty("java.home");
        var visitedFiles = new HashSet<String>();

        for (var url : urls)
        {
            try
            {
                var protocol = url.getProtocol();

                if (protocol.equals("file"))
                {
                    var file = FileSWL.of(url);
                    var filePath = file.getCanonicalFile().toString();

                    if (visitedFiles.contains(filePath))
                        continue;

                    visitedFiles.add(filePath);

                    if (file.isFile())
                    {
                        if (javaHome != null)
                        {
                            var javaHomeDir = FileSWL.of(javaHome);
                            if (file.isIn(javaHomeDir))
                                continue;
                        }

                        readJar(file.as().zip());
                    }
                    else
                        readDir(file);

                }
                else if (protocol.equals("jar"))
                {
                    var connection = (JarURLConnection) url.openConnection();
                    var jarFile = connection.getJarFile();
                    readJar(jarFile);
                }
            }
            catch (Throwable e)
            {
                exceptionsHandler.handle()
                        .message("Error while reading class scanner url:", url)
                        .tag(Exceptions.tagShortPrint)
                        .throwable(e)
                        .start()
                ;
            }
        }

        classEntries = classEntries.exStream()
                .distinctBy(ClassToScanEntry::getName)
                .toExList()
        ;
    }

    public void readJar(ZipFile zipFile)
    {
        if (zipFile == null)
            return;

        CollectionsSWL.exStream(zipFile.entries())
                .filter((e) -> e.getName().endsWith(".class"))
                .map((e) -> {
                    var className = getClassNameFromLocation(e.getName());
                    return new ClassToScanEntry()
                            .setName(className)
                            .setBytesFun(() -> StreamsUtils.readAll(zipFile.getInputStream(e)))
                    ;
                })
                .each(classEntries::add)
        ;
    }

    public void readDir(FileSWL dir)
    {
        if (dir == null)
            return;

        dir.findAllSubfiles()
                .filter((f) -> f.getExtension().equals("class"))
                .map((f) -> {
                    var fLocalPath = f.getLocalPath(dir);
                    var className = getClassNameFromLocation(fLocalPath);

                    return new ClassToScanEntry()
                            .setName(className)
                            .setBytesFun(() -> f.in().readAll())
                    ;
                })
                .each(classEntries::add)
        ;
    }

    public String getClassNameFromLocation(String loc)
    {
       return StringUtils.subString(0, ".class".length(), loc.replace("/", "."));
    }

    @Override
    public ExtendedStream<ClassToScanEntry> findClasses(String packageName) throws Throwable
    {
        onceRunner.apply(this::loadClasses);
        return classEntries.exStream();
    }
}
