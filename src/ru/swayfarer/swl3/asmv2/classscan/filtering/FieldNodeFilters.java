package ru.swayfarer.swl3.asmv2.classscan.filtering;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.tree.FieldNode;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.filers.ModifierFilters;
import ru.swayfarer.swl3.reflection.filers.StringsFiltering;

@Getter
@Setter
@Accessors(chain = true)
public class FieldNodeFilters
{
    @Internal
    @NonNull
    public AsmUtilsV2 asmUtils;

    public FieldNodeFilters(@NonNull AsmUtilsV2 asmUtils)
    {
        this.asmUtils = asmUtils;
    }

    public StringsFiltering<FieldNode> name()
    {
        return new StringsFiltering<>((field) -> CollectionsSWL.list(field.name));
    }

    public TypeFilters<FieldNode> type()
    {
        return new TypeFilters<>((field) -> Type.getType(field.desc), asmUtils);
    }

    public AnnotationNodeFilters<FieldNode> annotations()
    {
        return new AnnotationNodeFilters<>((node) -> asmUtils.annotations().findAll(node));
    }

    public AnnotationNodeFilters<FieldNode> aliasAnnotations()
    {
        return new AnnotationNodeFilters<>((node) -> asmUtils.annotations().findAliases(node));
    }

    public ModifierFilters<FieldNode> mod()
    {
        return new ModifierFilters<>((node) -> node.access);
    }
}
