package ru.swayfarer.swl3.asmv2.classscan.filtering;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.filers.ModifierFilters;
import ru.swayfarer.swl3.reflection.filers.StringsFiltering;

@Getter
@Setter
@Accessors(chain = true)
public class MethodNodeFilters
{
    @Internal
    @NonNull
    public AsmUtilsV2 asmUtils;

    public MethodNodeFilters()
    {
        this(AsmUtilsV2.getInstance());
    }

    public MethodNodeFilters(@NonNull AsmUtilsV2 asmUtils)
    {
        this.asmUtils = asmUtils;
    }

    public StringsFiltering<MethodNode> name()
    {
        return new StringsFiltering<>((methodNode) -> CollectionsSWL.list(methodNode.name));
    }

    public ModifierFilters<MethodNode> mod()
    {
        return new ModifierFilters<>((methodNode) -> methodNode.access);
    }

    public AnnotationNodeFilters<MethodNode> annotations()
    {
        return new AnnotationNodeFilters<>((methodNode) -> {
            if (methodNode == null)
                return CollectionsSWL.list();

            return asmUtils.annotations().findAll(methodNode);
        });
    }

    public TypeFilters<MethodNode> returnType()
    {
        return new TypeFilters<>((node) -> Type.getReturnType(node.desc), asmUtils);
    }

    public AnnotationNodeFilters<MethodNode> annotationsAliases()
    {
        return new AnnotationNodeFilters<>((methodNode) -> {
            if (methodNode == null)
                return CollectionsSWL.list();

            return asmUtils.annotations().findAliases(methodNode);
        });
    }

    public IFunction1<MethodNode, Boolean> constructors()
    {
        return (node) -> node != null && node.name.equals("<init>");
    }

    public IFunction1<MethodNode, Boolean> classInits()
    {
        return (node) -> node != null && node.name.equals("<clinit>");
    }
}
