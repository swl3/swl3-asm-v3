package ru.swayfarer.swl3.asmv2.classscan.filtering;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.tree.AnnotationNode;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.reflection.filers.AbstractFiltering;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class AnnotationNodeFilters<Object_Type> extends AbstractFiltering<Object_Type, ExtendedList<AnnotationNode>>
{
    public IFunction1<Object_Type, Object_Type> getParentFun = (o) -> null;

    public AnnotationNodeFilters(IFunction1<Object_Type, ExtendedList<AnnotationNode>> getValueFun)
    {
        super(getValueFun);
    }

    public IFunction1<Object_Type, Boolean> contains(Class<?>... annotationTypes)
    {
        return contains(ExtendedStream.of(annotationTypes).map(Type::getType).toArray(Type.class));
    }

    public IFunction1<Object_Type, Boolean> contains(Type... annotationTypes)
    {
        var annotationDescsList = CollectionsSWL.list(annotationTypes).map(Type::getDescriptor);

        return nonNullFilter((annotations) -> {
            for (var annotation : annotations)
            {
                if (annotationDescsList.contains(annotation.desc))
                    return true;
            }

            return false;
        });
    }

    public static <T> AnnotationNodeFilters<T> ofList(IFunction1<T, List<AnnotationNode>> listFun)
    {
        return new AnnotationNodeFilters<>((obj) -> {
            var list = listFun.apply(obj);
            if (list == null)
                return null;

            return CollectionsSWL.list(list);
        });
    }
}
