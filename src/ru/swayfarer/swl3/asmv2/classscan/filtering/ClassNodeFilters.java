package ru.swayfarer.swl3.asmv2.classscan.filtering;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.filers.AbstractFiltering;
import ru.swayfarer.swl3.reflection.filers.ModifierFilters;
import ru.swayfarer.swl3.reflection.filers.StringFilters;

@Getter
@Setter
@Accessors(chain = true)
public class ClassNodeFilters<Object_Type> extends AbstractFiltering<Object_Type, ClassNode>
{
    @NonNull
    @Internal
    public AsmUtilsV2 asmUtils;

    @NonNull
    @Internal
    public IFunction1<Object_Type, ClassNode> getValueFun;

    public ClassNodeFilters(AsmUtilsV2 asmUtils, IFunction1<Object_Type, ClassNode> getValueFun)
    {
        super(getValueFun);
        this.asmUtils = asmUtils;
        this.getValueFun = getValueFun;
    }

    public StringFilters<Object_Type> name()
    {
        return new StringFilters<>((classNode) -> asmUtils.classes().getClassName(getValueFun.apply(classNode)));
    }

    public StringFilters<Object_Type> internalName()
    {
        return new StringFilters<>((classNode) -> getValueFun.apply(classNode).name);
    }

    public ModifierFilters<Object_Type> mod()
    {
        return new ModifierFilters<>((classNode) -> getValueFun.apply(classNode).access);
    }

    public AnnotationNodeFilters<Object_Type> annotations()
    {
        return AnnotationNodeFilters.ofList((classNode) -> asmUtils.annotations().findAll(getValueFun.apply(classNode)));
    }

    public AnnotationNodeFilters<Object_Type> aliasAnnotations()
    {
        return AnnotationNodeFilters.ofList((classNode) -> asmUtils.annotations().findAliases(getValueFun.apply(classNode)));
    }

    public IFunction1<Object_Type, Boolean> implement(Class<?>... interfaces)
    {
        return implement(ExtendedStream.of(interfaces).map(Type::getType).toArray(Type.class));
    }

    public IFunction1<Object_Type, Boolean> implement(Type... interfaces)
    {
        var interfacesList = CollectionsSWL.list(interfaces).map(Type::getInternalName);

        return (obj) -> {
            var classNode = asmUtils.classes().checkModifySafeCache(getValueFun.apply(obj));
            var parentClassNode = classNode;

            while (parentClassNode != null)
            {
                if (interfacesList.containsSome(classNode.interfaces))
                    return true;

                parentClassNode = asmUtils.classes().findByInternalName(parentClassNode.superName).orNull();
            }

            return false;
        };
    }

    public IFunction1<Object_Type, Boolean> extend(Class<?>... parents)
    {
        return extend(ExtendedStream.of(parents).map(Type::getType).toArray(Type.class));
    }

    public IFunction1<Object_Type, Boolean> extend(Type... parents)
    {
        var parentsList = CollectionsSWL.list(parents).map(Type::getInternalName);

        return (obj) -> {
            var classNode = asmUtils.classes().checkModifySafeCache(getValueFun.apply(obj));

            if (classNode == null)
                return false;

            if (classNode.superName == null)
                return false;

            var compareType = Type.getObjectType(classNode.superName);

            while (compareType != null)
            {
                if (parentsList.contains(compareType.getInternalName()))
                    return true;

                var compareClassNode = asmUtils.classes().findByType(compareType).orNull();

                if (compareClassNode == null)
                    return false;

                if (compareClassNode.superName == null)
                    return false;

                compareType = Type.getObjectType(compareClassNode.superName);
            }

            return false;
        };
    }

    public IFunction1<Object_Type, Boolean> equal(Class<?>... classes)
    {
        return equal(ExtendedStream.of(classes).map(Type::getType).toArray(Type.class));
    }

    public IFunction1<Object_Type, Boolean> equal(Type... types)
    {
        var typesList = ExtendedStream.of(types)
                .nonNull()
                .toExList()
        ;

        return nonNullFilter((node) -> typesList.contains(Type.getObjectType(node.name)));
    }

    public IFunction1<Object_Type, Boolean> assignableFor(Type... types)
    {
        return equal(types).or(implement(types)).or(extend(types));
    }

    public IFunction1<Object_Type, Boolean> assignableFor(Class<?>... classes)
    {
        return equal(classes).or(implement(classes)).or(extend(classes));
    }
}
