package ru.swayfarer.swl3.asmv2.classscan.filtering;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.filers.AbstractFiltering;

@Getter
@Setter
@Accessors(chain = true)
public class TypeFilters<Object_Type> extends AbstractFiltering<Object_Type, Type>
{
    @Internal
    @NonNull
    public AsmUtilsV2 asmUtils;

    public TypeFilters(IFunction1<Object_Type, Type> getValueFun, AsmUtilsV2 asmUtils)
    {
        super(getValueFun);
        this.asmUtils = asmUtils;
    }

    public IFunction1<Object_Type, Boolean> equal(Type... types)
    {
        var typesList = CollectionsSWL.list(types);
        return nonNullFilter(typesList::contains);
    }

    public IFunction1<Object_Type, Boolean> arrays()
    {
        return nonNullFilter((type) -> type.getSort() == Type.ARRAY);
    }

    public ClassNodeFilters<Object_Type> asClass()
    {
        return new ClassNodeFilters<>(
                asmUtils,
                (obj) -> asmUtils.classes().findByType(getValueFun.apply(obj)).orNull()
        );
    }
}
