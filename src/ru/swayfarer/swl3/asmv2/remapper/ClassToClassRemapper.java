package ru.swayfarer.swl3.asmv2.remapper;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.commons.Remapper;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class ClassToClassRemapper extends Remapper
{
    @NonNull
    @Internal
    public FunctionsChain1<Type, Type> newTypeFuns = new FunctionsChain1<>();

    @Override
    public String map(String internalName)
    {
        return super.map(internalName);
    }
}
