package ru.swayfarer.swl3.asmv2.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.ClassVisitor;
import ru.swayfarer.swl3.asmv2.asm.MethodVisitor;
import ru.swayfarer.swl3.asmv2.asm.Opcodes;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.commons.AdviceAdapter;
import ru.swayfarer.swl3.asmv2.classscan.filtering.MethodNodeFilters;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.asmv2.visitor.ExtendedAdviceAdapter;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.markers.Internal;

import java.util.concurrent.atomic.AtomicBoolean;

@Getter
@Setter
@Accessors(chain = true)
public class MethodNodeAsmUtils
{
    @Internal
    @NonNull
    public AsmUtilsV2 asmUtils;

    @Internal
    @NonNull
    public MethodParametersAsmUtils parametersAsmUtils;

    public MethodNodeAsmUtils(@NonNull AsmUtilsV2 asmUtils)
    {
        this.asmUtils = asmUtils;
        parametersAsmUtils = new MethodParametersAsmUtils(asmUtils);
    }

    public ExtendedStream<MethodNode> stream(ClassNode classNode)
    {
        if (classNode == null || classNode.methods == null)
            return ExtendedStream.empty();

        return new ExtendedStream<>(asmUtils.classes().checkModifySafeCache(classNode).methods::stream);
    }

    public ExtendedStream<MethodNode> stream(Type type)
    {
        if (type == null || type.getSort() != Type.OBJECT)
            return ExtendedStream.empty();

        var classNode = asmUtils.classes().findByType(type).orNull();
        return classNode == null ? ExtendedStream.empty() : stream(classNode);
    }

    public ExtendedStream<MethodNode> stream(Class<?> type)
    {
        if (type == null)
            return ExtendedStream.empty();

        return stream(Type.getType(type));
    }

    public MethodParametersAsmUtils parameters()
    {
        return parametersAsmUtils;
    }

    public MethodNodeFilters filters()
    {
        return new MethodNodeFilters(asmUtils);
    }

    public boolean isContructor(MethodNode methodNode)
    {
        if (methodNode == null)
            return false;

        return methodNode.name.equals("<init>");
    }

    public boolean isPrimartyConstructor(Type methodOwner, MethodNode methodNode)
    {
        if (!isContructor(methodNode))
            return false;

        return !isSecondaryConstructor(methodOwner, methodNode);
    }

    public ExtendedAdviceAdapter visitAdviceAdapter(ClassVisitor cv, int access, String name, String desc)
    {
        var mv = cv.visitMethod(access, name, desc, null, null);
        return new ExtendedAdviceAdapter(Opcodes.ASM_CURRENT, mv, access, name, desc);
    }

    public boolean isSecondaryConstructor(Type methodOwner, MethodNode methodNode)
    {
        if (!isContructor(methodNode))
            return false;

        var isSecondary = new AtomicBoolean();

        // The method visitor which compares NEW instructions and INVOKESPECIAL method call for method owner
        // If constructor is not primary, INVOKESPECIAL with this method owner will be at 0 NEW instructions.

        MethodVisitor mv = new MethodVisitor(Opcodes.ASM_CURRENT)
        {
            int newObjectsCount = 0;

            @Override
            public void visitInsn(int opcode)
            {
                if (opcode == Opcodes.NEW)
                    newObjectsCount ++;

                super.visitInsn(opcode);
            }

            @Override
            public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface)
            {
                if (opcode == Opcodes.INVOKESPECIAL)
                {
                    if (newObjectsCount == 0 && methodOwner.getInternalName().equals(owner))
                    {
                        isSecondary.set(true);
                    }

                    if (newObjectsCount > 0)
                        newObjectsCount --;
                }

                super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
            }
        };

        return isSecondary.get();
    }
}
