package ru.swayfarer.swl3.asmv2.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class AsmUtilsV2
{
    public static AsmUtilsV2 INSTANCE = new AsmUtilsV2();

    @NonNull
    @Internal
    public ClassNodeAsmUtils classNodeAsmUtils = new ClassNodeAsmUtils()
            .setAsmUtils(this)
    ;

    @NonNull
    @Internal
    public AnnotationNodeAsmUtils annotationNodeAsmUtils = new AnnotationNodeAsmUtils(this);

    @NonNull
    @Internal
    public FieldNodeAsmUtils fieldNodeAsmUtils = new FieldNodeAsmUtils(this);

    @NonNull
    @Internal
    public MethodNodeAsmUtils methodNodeAsmUtils = new MethodNodeAsmUtils(this);

    @NonNull
    @Internal
    public TypesAsmUtils typesAsmUtils = new TypesAsmUtils(this);

    @NonNull
    @Internal
    public AsmCommonUtils commonUtils = new AsmCommonUtils(this);

    public ClassNodeAsmUtils classes()
    {
        return classNodeAsmUtils;
    }

    public FieldNodeAsmUtils fields()
    {
        return fieldNodeAsmUtils;
    }

    public MethodNodeAsmUtils methods()
    {
        return methodNodeAsmUtils;
    }

    public AnnotationNodeAsmUtils annotations()
    {
        return annotationNodeAsmUtils;
    }

    public TypesAsmUtils types()
    {
        return typesAsmUtils;
    }

    public AsmCommonUtils commons()
    {
        return commonUtils;
    }

    public static AsmUtilsV2 getInstance()
    {
        return INSTANCE;
    }
}
