package ru.swayfarer.swl3.asmv2.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.classscan.AssignableAnnotationsScanner;
import ru.swayfarer.swl3.asmv2.asm.tree.AnnotationNode;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.asm.tree.FieldNode;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.collections.list.ExtendedList;

@Getter
@Setter
@Accessors(chain = true)
public class AnnotationNodeAsmUtils
{
    @NonNull
    public AssignableAnnotationsScanner assignableAnnotationsFinder;

    @NonNull
    public AnnotationPropertyAsmUtils propertyUtils;

    @NonNull
    public AsmUtilsV2 asmUtils;

    public AnnotationNodeAsmUtils(AsmUtilsV2 asmUtilsV2)
    {
        this.asmUtils = asmUtilsV2;
        this.assignableAnnotationsFinder = new AssignableAnnotationsScanner(asmUtilsV2);
        this.propertyUtils = new AnnotationPropertyAsmUtils(asmUtilsV2);
    }

    public AnnotationPropertyAsmUtils properties()
    {
        return propertyUtils;
    }

    public ExtendedList<AnnotationNode> findByType(Class<?> type, ClassNode classNode)
    {
        return findByType(Type.getType(type), classNode);
    }

    public ExtendedList<AnnotationNode> findByType(Type type, ClassNode classNode)
    {
        return findAll(classNode)
                .filter((an) -> an.desc.equals(type.getDescriptor()))
        ;
    }

    public ExtendedList<AnnotationNode> findByType(Class<?> type, MethodNode classNode)
    {
        return findByType(Type.getType(type), classNode);
    }

    public ExtendedList<AnnotationNode> findByType(Type type, MethodNode methodNode)
    {
        return findAll(methodNode)
                .filter((an) -> an.desc.equals(type.getDescriptor()))
        ;
    }

    public ExtendedList<AnnotationNode> findByType(Class<?> type, FieldNode classNode)
    {
        return findByType(Type.getType(type), classNode);
    }

    public ExtendedList<AnnotationNode> findByType(Type type, FieldNode fieldNode)
    {
        return findAll(fieldNode)
                .filter((an) -> an.desc.equals(type.getDescriptor()))
        ;
    }

    public ExtendedList<AnnotationNode> findAll(ClassNode classNode)
    {
        classNode = asmUtils.classes().checkModifySafeCache(classNode);

        var annotations = new ExtendedList<AnnotationNode>();

        if (classNode.visibleAnnotations != null)
            annotations.addAll(classNode.visibleAnnotations);

        if (classNode.invisibleAnnotations != null)
            annotations.addAll(classNode.invisibleAnnotations);

        return annotations;
    }

    public ExtendedList<AnnotationNode> findAll(MethodNode methodNode)
    {
        var annotations = new ExtendedList<AnnotationNode>();

        if (methodNode.visibleAnnotations != null)
            annotations.addAll(methodNode.visibleAnnotations);

        if (methodNode.invisibleAnnotations != null)
            annotations.addAll(methodNode.invisibleAnnotations);

        return annotations;
    }

    public ExtendedList<AnnotationNode> findAll(FieldNode fieldNode)
    {
        var annotations = new ExtendedList<AnnotationNode>();

        if (fieldNode.visibleAnnotations != null)
            annotations.addAll(fieldNode.visibleAnnotations);

        if (fieldNode.invisibleAnnotations != null)
            annotations.addAll(fieldNode.invisibleAnnotations);

        return annotations;
    }

    public ExtendedList<AnnotationNode> findAliases(ClassNode classNode)
    {
        classNode = asmUtils.classes().checkModifySafeCache(classNode);
        return assignableAnnotationsFinder.findAnnotations(classNode);
    }

    public ExtendedList<AnnotationNode> findAliases(MethodNode methodNode)
    {
        return assignableAnnotationsFinder.findAnnotations(methodNode);
    }

    public ExtendedList<AnnotationNode> findAliases(FieldNode node)
    {
        return assignableAnnotationsFinder.findAnnotations(node);
    }
}
