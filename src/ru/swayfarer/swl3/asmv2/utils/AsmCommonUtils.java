package ru.swayfarer.swl3.asmv2.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.commons.Method;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class AsmCommonUtils
{
    @Internal
    @NonNull
    public AsmUtilsV2 asmUtils;

    public AsmCommonUtils(@NonNull AsmUtilsV2 asmUtils)
    {
        this.asmUtils = asmUtils;
    }

    public Method method(String name, Class<?> returnType, Class<?>... paramTypes)
    {
        return new Method(name, Type.getType(returnType), ExtendedStream.of(paramTypes)
                .map(Type::getType)
                .toArray(Type.class)
        );
    }
}
