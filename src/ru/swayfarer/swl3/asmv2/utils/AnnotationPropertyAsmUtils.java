package ru.swayfarer.swl3.asmv2.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.tree.AnnotationNode;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.markers.Internal;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class AnnotationPropertyAsmUtils
{
    @Internal
    @NonNull
    public AsmUtilsV2 asmUtils;

    public AnnotationPropertyAsmUtils(@NonNull AsmUtilsV2 asmUtils)
    {
        this.asmUtils = asmUtils;
    }

    public <T> ExtendedOptional<T> get(AnnotationNode annotationNode, String name, Class<T> cl)
    {
        if (annotationNode.values == null)
            return ExtendedOptional.empty();

        for (int i = 0, n = annotationNode.values.size(); i < n; i += 2)
        {
            String paramName = (String) annotationNode.values.get(i);
            Object paramValue = annotationNode.values.get(i + 1);

            if (paramName.equals(name))
            {
                paramValue = readAsmValue(paramValue);

                if (cl.isAssignableFrom(paramValue.getClass()))
                {
                    return ExtendedOptional.of((T) paramValue);
                }
            }
        }

        return ExtendedOptional.empty();
    }

    public <T> ExtendedOptional<ExtendedList<T>> getList(AnnotationNode annotationNode, String name, Class<T> cl)
    {
        if (annotationNode.values == null)
            return ExtendedOptional.empty();

        for (int i = 0, n = annotationNode.values.size(); i < n; i += 2)
        {
            String paramName = (String) annotationNode.values.get(i);
            Object paramValue = annotationNode.values.get(i + 1);

            if (paramName.equals(name))
            {
                paramValue = readAsmValue(paramValue);

                if (paramValue instanceof List)
                {
                    return ExtendedOptional.of(CollectionsSWL.list((List) paramValue));
                }
            }
        }

        return get(annotationNode, name, cl).map(CollectionsSWL::list);
    }

    public Object readAsmValue(Object obj)
    {
        if (obj instanceof String[])
            return ((String[]) obj) [1];

        if (obj instanceof List)
        {
            var list = (List<?>) obj;

            return ExtendedStream.of(list)
                    .map(this::readAsmValue)
                    .toList()
            ;
        }

        return obj;
    }
}
