package ru.swayfarer.swl3.asmv2.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.Label;
import ru.swayfarer.swl3.asmv2.asm.MethodVisitor;
import ru.swayfarer.swl3.asmv2.asm.Opcodes;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.markers.Internal;

/**
 * The utilities for types conversion
 */
@Getter
@Setter
@Accessors(chain = true)
public class TypeConversionAsmUtils
{
    /** The asm utils */
    @Internal
    @NonNull
    public AsmUtilsV2 asmUtils;

    /**
     * The list of box entries ({@link BoxEntry}) <br>
     * Contains all available box/unbox pairs for {@link #box(MethodVisitor, Type)} and {@link #unbox(MethodVisitor, Type)} operations
     */
    @Internal
    public ExtendedList<BoxEntry> boxingConfig = new ExtendedList<>();

    public TypeConversionAsmUtils(AsmUtilsV2 asmUtils)
    {
        this.asmUtils = asmUtils;

        boxingConfig.addAll(
                BoxEntry.of(Boolean.class, boolean.class),
                BoxEntry.of(Character.class, char.class),

                BoxEntry.of(Byte.class, byte.class),
                BoxEntry.of(Short.class, short.class),
                BoxEntry.of(Integer.class, int.class),
                BoxEntry.of(Long.class, long.class),
                BoxEntry.of(Float.class, float.class),
                BoxEntry.of(Double.class, double.class)
        );
    }

    /**
     * Do conversions from source to target types. Applies boxing and checkcast operations if needed.
     * @param mv {@link MethodVisitor} to apply conversions
     * @param from Source type
     * @param to Target type
     */
    public void convert(MethodVisitor mv, Type from, Type to)
    {
        var toVoid = Type.VOID_TYPE.equals(to);
        var fromVoid = Type.VOID_TYPE.equals(from);

        if ((toVoid || fromVoid) && !(toVoid && fromVoid))
            ExceptionsUtils.throwException(new IllegalArgumentException("Can't convert anything from/to void!"));

        var sourcePrimitive = asmUtils.types().isPrimitive(from);
        var targetPrimitive = asmUtils.types().isPrimitive(to);
        var needBoxing = sourcePrimitive && !targetPrimitive;
        var needUnboxing = !sourcePrimitive && targetPrimitive;

        // If converting from raw Object to primitive type than we need to cast source object to boxed target type and apply unbox
        if (needUnboxing && Type.getType(Object.class).equals(from))
        {
            // final version for exception lambda
            var boxEntry = findBoxingFor(to).orElseThrow(() -> new IllegalStateException("Can't find boxing pair for type " + to));

            mv.visitTypeInsn(Opcodes.CHECKCAST, boxEntry.getBoxed().getInternalName());
            from = boxEntry.getBoxed();
        }

        if (needBoxing)
        {
            box(mv, from);
        }

        if (needUnboxing)
        {
            unbox(mv, from);
        }

        if (!targetPrimitive)
        {
            mv.visitTypeInsn(Opcodes.CHECKCAST, "java/lang/Object");
            mv.visitTypeInsn(Opcodes.CHECKCAST, to.getInternalName());
        }
    }

    /**
     * Unbox type
     * @param mv {@link MethodVisitor} to apply unboxing
     * @param type Wrapper type to unbox
     * @throws IllegalArgumentException If unboxing was not found for target type
     */
    public void unbox(MethodVisitor mv, Type type)
    {
        var unboxing = findUnboxingFor(type).orElseThrow(() -> new IllegalArgumentException("Can't find unboxing for type " + type));
        var boxed = unboxing.getBoxed();
        var unboxed = unboxing.getUnboxed();

        mv.visitInsn(Opcodes.DUP);

        // Ensure that boxed value is not null
        var lblIfNotNull = new Label();
        mv.visitJumpInsn(Opcodes.IFNONNULL, lblIfNotNull);
        mv.visitTypeInsn(Opcodes.NEW, "java/lang/NullPointerException");
        mv.visitInsn(Opcodes.DUP);
        mv.visitLdcInsn("Can't unwrap " + unboxed.getClassName() + " from null!");
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, "java/lang/NullPointerException", "<init>", "(Ljava/lang/String;)V", false);
        mv.visitInsn(Opcodes.ATHROW);
        mv.visitLabel(lblIfNotNull);

        mv.visitMethodInsn(
                Opcodes.INVOKEVIRTUAL,
                boxed.getInternalName(),
                unboxed.getClassName() + "Value",
                "()" + unboxed.getDescriptor(),
                false
        );
    }

    /**
     * Box type
     * @param mv {@link MethodVisitor} to apply boxing
     * @param type Primitive type to box
     * @throws IllegalArgumentException If unboxing was not found for target type
     */
    public void box(MethodVisitor mv, Type type)
    {
        var boxing = findBoxingFor(type).orElseThrow(() -> new IllegalArgumentException("Can't find boxing for type " + type));
        var boxed = boxing.getBoxed();
        var unboxed = boxing.getUnboxed();

        mv.visitMethodInsn(
                Opcodes.INVOKESTATIC,
                boxed.getInternalName(),
                "valueOf",
                "(" + unboxed.getDescriptor() + ")" + boxed.getDescriptor(),
                false
        );
    }

    @Internal
    public ExtendedOptional<BoxEntry> findUnboxingFor(Type boxedType)
    {
        return boxingConfig.exStream()
                .filter((entry) -> entry.boxed.equals(boxedType))
                .findAny()
        ;
    }

    @Internal
    public ExtendedOptional<BoxEntry> findBoxingFor(Type unboxedType)
    {
        return boxingConfig.exStream()
                .filter((entry) -> entry.unboxed.equals(unboxedType))
                .findAny()
        ;
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BoxEntry {

        @Internal
        @NonNull
        public Type boxed;

        @Internal
        @NonNull
        public Type unboxed;

        public static BoxEntry of(Class<?> boxed, Class<?> unboxed)
        {
            return new BoxEntry(Type.getType(boxed), Type.getType(unboxed));
        }
    }
}
