package ru.swayfarer.swl3.asmv2.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.StringUtils;

@Getter
@Setter
@Accessors(chain = true)
public class TypesAsmUtils
{
    @Internal
    @NonNull
    public AsmUtilsV2 asmUtils;

    @Internal
    @NonNull
    public TypeConversionAsmUtils conversionAsmUtils;

    public TypesAsmUtils(@NonNull AsmUtilsV2 asmUtils)
    {
        this.asmUtils = asmUtils;
        conversionAsmUtils = new TypeConversionAsmUtils(asmUtils);
    }

    public boolean isPrimitive(Type type)
    {
        return type.getSort() >= 0 && type.getSort() <= 8;
    }

    public TypeConversionAsmUtils conversions()
    {
        return conversionAsmUtils;
    }

    public Type findObjectTypeByName(String name)
    {
        if (StringUtils.isBlank(name))
            return null;

        if (name.startsWith("L") && name.endsWith(";"))
        {
            name = StringUtils.subString(1, 1, name);
        }

        name = name.replace('.', '/');

        return Type.getObjectType(name);
    }
}
