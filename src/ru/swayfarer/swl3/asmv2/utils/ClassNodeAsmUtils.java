package ru.swayfarer.swl3.asmv2.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.ClassReader;
import ru.swayfarer.swl3.asmv2.asm.ClassVisitor;
import ru.swayfarer.swl3.asmv2.asm.ClassWriter;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.commons.Method;
import ru.swayfarer.swl3.asmv2.classnode.ClassNodeCache;
import ru.swayfarer.swl3.asmv2.classscan.filtering.ClassNodeFilters;
import ru.swayfarer.swl3.asmv2.asm.tree.AnnotationNode;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.asm.tree.TypeAnnotationNode;
import ru.swayfarer.swl3.asmv2.asm.tree.Util;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.ExceptionOptional;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * The Utils for work with class nodes
 */
@Getter
@Setter
@Accessors(chain = true)
public class ClassNodeAsmUtils
{
    /** The root asm utils */
    @NonNull
    public AsmUtilsV2 asmUtils;

    /** The cache of {@link ClassNode}'s*/
    @NonNull
    public ClassNodeCache classNodeCache = new ClassNodeCache();

    /**
     * The cache that used while class node is under {@link #modify(ClassNode, ClassVisitor)} method processing. <br>
     * Modify clear class node data and transformers does not get node contents normally. Cache stores clone of original class node contents while
     * processing does not complete.
     */
    @NonNull
    public ExtendedMap<ClassNode, ClassNode> modifySafeCache = new ExtendedMap<>().synchronize();

    /**
     *  Filters for {@link ClassNode} uses this asm utils instance
     */
    public ClassNodeFilters<ClassNode> filters()
    {
        return new ClassNodeFilters<>(asmUtils, (obj) -> obj);
    }

    /**
     * Get java class-name from class node. e.g. java.lang.Object or sun.misc.Unsafe
     * @param classNode Target class node to get java name
     * @return Java-styled class name e.g. java.lang.Object
     */
    public String getClassName(ClassNode classNode)
    {
        return classNode.name.replace("/", ".");
    }

    /**
     * Find {@link ClassNode} by descriptor name (e.g. <code>Ljava/lang/Object;</code>). Uses caching. <br>
     * If cached {@link ClassNode} exists for given internal name than it will be returned.
     * @param descriptor Class descriptor
     * @return {@link ClassNode} instance or empty optional if not found
     */
    public ExtendedOptional<ClassNode> findByDescriptor(String descriptor)
    {
        if (StringUtils.isBlank(descriptor))
            return ExtendedOptional.empty();

        return findByType(Type.getType(descriptor));
    }

    /**
     * Find {@link ClassNode} by internal name (e.g. <code>java/lang/Object</code>). Uses caching. <br>
     * If cached {@link ClassNode} exists for given internal name than it will be returned.
     * @param internalName Class internal name
     * @return {@link ClassNode} instance or empty optional if not found
     */
    public ExtendedOptional<ClassNode> findByInternalName(String internalName)
    {
        if (StringUtils.isBlank(internalName))
            return ExtendedOptional.empty();

        return ExceptionOptional.ofAction(() -> getClassNodeCache().getOrCreateClassNode(internalName));
    }

    /**
     * Find {@link ClassNode} by Java class. Uses caching. <br>
     * If cached {@link ClassNode} exists for given class than it will be returned.
     * @param type Java target class
     * @return {@link ClassNode} instance or empty optional if not found
     */
    public ExtendedOptional<ClassNode> findByClass(Class<?> type)
    {
        if (type == null)
            return ExtendedOptional.empty();

        return findByType(Type.getType(type));
    }

    /**
     * Find {@link ClassNode} by type. Uses caching. <br>
     * If cached {@link ClassNode} exists for given type than it will be returned.
     * @param type Class node type
     * @return {@link ClassNode} instance or empty optional if not found
     */
    public ExtendedOptional<ClassNode> findByType(Type type)
    {
        if (type == null)
            return ExtendedOptional.empty();

        if (asmUtils.types().isPrimitive(type))
            return ExtendedOptional.empty();

        return ExceptionOptional.ofAction(() -> getClassNodeCache().getOrCreateClassNode(type.getInternalName())).map((node) -> modifySafeCache.getOrDefault(node, node));
    }

    /**
     * Get raw-bytes class node representation with frames computing ({@link ClassWriter#COMPUTE_FRAMES})
     * @param classNode Target class node
     * @return if given class node is null than null else class bytes
     */
    public byte[] getClassBytes(ClassNode classNode)
    {
        return getClassBytes(ClassWriter.COMPUTE_FRAMES, classNode);
    }

    /**
     * Get raw-bytes class node representation with class writer options <br>
     *
     * @param classNode Target class node
     * @param flags Class writer flags. See {@link ClassWriter#COMPUTE_FRAMES} or {@link ClassWriter#COMPUTE_MAXS}
     * @return if given class node is null than null else class bytes
     */
    public byte[] getClassBytes(int flags, ClassNode classNode)
    {
        if (classNode == null)
            return null;

        var cw = new ClassWriter(flags);
        classNode.accept(cw);
        return cw.toByteArray();
    }

    /**
     * Get modify-safe cached value or original class node if not cached. <br>
     * See {@link #modifySafeCache}
     * @param classNode Target class node
     * @return ClassNode instance
     */
    public ClassNode checkModifySafeCache(ClassNode classNode)
    {
        return modifySafeCache.getOrDefault(classNode, classNode);
    }

    public void modify(ClassNode classNode, ClassVisitor classVisitor)
    {
        var parent = classVisitor;
        var nodeFound = false;

        while (parent != null)
        {
            if (parent == classNode)
            {
                nodeFound = true;
                break;
            }

            parent = parent.getParent();
        }

        ExceptionsUtils.IfNot(
                nodeFound,
                IllegalArgumentException.class,
                "Class visitor must reference to class node! (See ClassVisitor::getParent)"
        );

        var modifyCachedValue = cloneRefs(classNode);
        modifySafeCache.put(classNode, modifyCachedValue);

        var bytes = getClassBytes(0, classNode);
        cleanClassNode(classNode);
        var classReader = new ClassReader(bytes);
        classReader.accept(classVisitor, ClassReader.EXPAND_FRAMES);

        modifySafeCache.remove(classNode);
    }

    public void cleanClassNode(ClassNode classNode)
    {
        cleanCollection(() -> classNode.interfaces);
        cleanCollection(() -> classNode.methods);
        cleanCollection(() -> classNode.fields);
        cleanCollection(() -> classNode.attrs);
        cleanCollection(() -> classNode.innerClasses);
        cleanCollection(() -> classNode.invisibleAnnotations);
        cleanCollection(() -> classNode.invisibleTypeAnnotations);
        cleanCollection(() -> classNode.visibleAnnotations);
        cleanCollection(() -> classNode.visibleTypeAnnotations);
        cleanCollection(() -> classNode.nestMembers);
    }

    public void cleanCollection(IFunction0<Collection<?>> collectionFun)
    {
        var collection = collectionFun.apply();
        if (collection != null)
        {
            collection.clear();
        }
    }

    /**
     * Modify ClassNode by ClassVisitor <br>
     * Rewrites class node by target class visitor.
     * <h3> Please note that class visitor must use class node as parent</h3>
     * See {@link #modifySafeCache} {@link #checkModifySafeCache(ClassNode)}
     * @param classNode Target class node to rewrite
     * @param classVisitor Target class visitor
     */
    public void modify2(ClassNode classNode, ClassVisitor classVisitor)
    {
        var modifyCachedValue = cloneRefs(classNode);
        modifySafeCache.put(classNode, modifyCachedValue);

//        classNode.name = null;
//        classNode.version = 0;
//        classNode.module = null;
//        classNode.superName = null;
//        classNode.access = 0;
//        classNode.nestHostClass = null;
//        classNode.outerClass = null;
//        classNode.outerMethod = null;
//        classNode.signature = null;
//        classNode.outerMethodDesc = null;
//        classNode.sourceDebug = null;

        // Visit the header.
        String[] interfacesArray = new String[classNode.interfaces.size()];
        classNode.interfaces.toArray(interfacesArray);
        classVisitor.visit(classNode.version, classNode.access, classNode.name, classNode.signature, classNode.superName, interfacesArray);
        // Visit the source.
        if (classNode.sourceFile != null || classNode.sourceDebug != null) {
            classVisitor.visitSource(classNode.sourceFile, classNode.sourceDebug);
        }
        // Visit the module.
        if (classNode.module != null) {
            var module = classNode.module;
            classNode.module = null;

            module.accept(classVisitor);
        }
        // Visit the nest host class.
        if (classNode.nestHostClass != null) {
            classVisitor.visitNestHost(classNode.nestHostClass);
        }
        // Visit the outer class.
        if (classNode.outerClass != null) {
            classVisitor.visitOuterClass(classNode.outerClass, classNode.outerMethod, classNode.outerMethodDesc);
        }
        // Visit the annotations.
        if (classNode.visibleAnnotations != null) {
            var visibleAnnotations = Util.copyAndClean(classNode.visibleAnnotations);

            for (int i = 0, n = visibleAnnotations.size(); i < n; ++i) {
                AnnotationNode annotation = visibleAnnotations.get(i);
                annotation.accept(classVisitor.visitAnnotation(annotation.desc, true));
            }
        }
        if (classNode.invisibleAnnotations != null) {
            var invisibleAnnotations = Util.copyAndClean(classNode.invisibleAnnotations);

            for (int i = 0, n = invisibleAnnotations.size(); i < n; ++i) {
                AnnotationNode annotation = invisibleAnnotations.get(i);
                annotation.accept(classVisitor.visitAnnotation(annotation.desc, false));
            }
        }
        if (classNode.visibleTypeAnnotations != null) {
            var visibleTypeAnnotations = Util.copyAndClean(classNode.visibleTypeAnnotations);

            for (int i = 0, n = visibleTypeAnnotations.size(); i < n; ++i) {
                TypeAnnotationNode typeAnnotation = visibleTypeAnnotations.get(i);
                typeAnnotation.accept(
                        classVisitor.visitTypeAnnotation(
                                typeAnnotation.typeRef, typeAnnotation.typePath, typeAnnotation.desc, true));
            }
        }
        if (classNode.invisibleTypeAnnotations != null) {
            var invisibleTypeAnnotations = Util.copyAndClean(classNode.invisibleTypeAnnotations);

            for (int i = 0, n = invisibleTypeAnnotations.size(); i < n; ++i) {
                TypeAnnotationNode typeAnnotation = invisibleTypeAnnotations.get(i);
                typeAnnotation.accept(
                        classVisitor.visitTypeAnnotation(
                                typeAnnotation.typeRef, typeAnnotation.typePath, typeAnnotation.desc, false));
            }
        }
        // Visit the non standard attributes.
        if (classNode.attrs != null) {
            var attrs = Util.copyAndClean(classNode.attrs);

            for (int i = 0, n = attrs.size(); i < n; ++i) {
                classVisitor.visitAttribute(attrs.get(i));
            }
        }
        // Visit the nest members.
        if (classNode.nestMembers != null) {
            var nestMembers = Util.copyAndClean(classNode.nestMembers);

            for (int i = 0, n = nestMembers.size(); i < n; ++i) {
                classVisitor.visitNestMember(nestMembers.get(i));
            }
        }
        // Visit the permitted subclasses.
        if (classNode.permittedSubclasses != null) {
            var permittedSubclasses = Util.copyAndClean(classNode.permittedSubclasses);

            for (int i = 0, n = permittedSubclasses.size(); i < n; ++i) {
                classVisitor.visitPermittedSubclass(permittedSubclasses.get(i));
            }
        }

        // Visit the inner classes.
        for (int i = 0, n = classNode.innerClasses.size(); i < n; ++i) {
            var innerClasses = Util.copyAndClean(classNode.innerClasses);

            if (i < innerClasses.size())
                innerClasses.get(i).accept(classVisitor);
        }
        // Visit the record components.
        if (classNode.recordComponents != null) {
            var recordComponents = Util.copyAndClean(classNode.recordComponents);

            for (int i = 0, n = recordComponents.size(); i < n; ++i) {
                recordComponents.get(i).accept(classVisitor);
            }
        }

        var fields = Util.copyAndClean(classNode.fields);

        // Visit the fields.
        for (int i = 0, n = fields.size(); i < n; ++i) {
            fields.get(i).accept(classVisitor);
        }

        var methods = Util.copyAndClean(classNode.methods);

        // Visit the methods.
        for (int i = 0, n = methods.size(); i < n; ++i) {
            methods.get(i).accept(classVisitor);
        }

        classVisitor.visitEnd();

        modifySafeCache.remove(classNode);
    }

    /**
     * Create class node instance that references to target class node params <br>
     * <h3>Note that it clones refs only, modification of contents of refs will affect all clones and original!</h3>
     * @param classNode Target class node
     * @return Ref-Cloned class node
     */
    public ClassNode cloneRefs(ClassNode classNode)
    {
        var ret = new ClassNode();

        ret.access = classNode.access;
        ret.module = classNode.module;
        ret.name = classNode.name;
        ret.superName = classNode.superName;

        ret.methods = cloneList(classNode.methods);
        ret.fields = cloneList(classNode.fields);

        ret.invisibleAnnotations = cloneList(classNode.invisibleAnnotations);
        ret.visibleAnnotations = cloneList(classNode.visibleAnnotations);

        ret.visibleTypeAnnotations = cloneList(classNode.visibleTypeAnnotations);
        ret.invisibleAnnotations = cloneList(classNode.invisibleAnnotations);
        ret.nestMembers = cloneList(classNode.nestMembers);
        ret.innerClasses = cloneList(classNode.innerClasses);
        ret.attrs = cloneList(classNode.attrs);

        ret.recordComponents = cloneList(classNode.recordComponents);

        return ret;
    }

    @Internal
    private <T> List<T> cloneList(List<T> list)
    {
        if (list == null)
            return null;

        return new ArrayList<>(list);
    }
}
