package ru.swayfarer.swl3.asmv2.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.classscan.filtering.FieldNodeFilters;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.asm.tree.FieldNode;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class FieldNodeAsmUtils
{
    @NonNull
    @Internal
    public AsmUtilsV2 asmUtils;

    public FieldNodeAsmUtils(@NonNull AsmUtilsV2 asmUtils)
    {
        this.asmUtils = asmUtils;
    }

    public ExtendedStream<FieldNode> stream(ClassNode classNode)
    {
        if (classNode == null || classNode.fields == null)
            return ExtendedStream.empty();


        return new ExtendedStream<>(asmUtils.classes().checkModifySafeCache(classNode).fields::stream);
    }

    public ExtendedStream<FieldNode> stream(Type type)
    {
        if (type.getSort() != Type.OBJECT)
            return ExtendedStream.empty();

        var classNode = asmUtils.classes().findByType(type).orNull();
        return classNode == null ? ExtendedStream.empty() : stream(classNode);
    }

    public FieldNodeFilters filters()
    {
        return new FieldNodeFilters(asmUtils);
    }
}
