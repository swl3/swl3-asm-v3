package ru.swayfarer.swl3.asmv2.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.classscan.MethodParameterNode;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.asmv2.asm.tree.ParameterNode;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.markers.Internal;

import java.lang.reflect.Modifier;
import java.util.concurrent.atomic.AtomicInteger;

@Getter
@Setter
@Accessors(chain = true)
public class MethodParametersAsmUtils
{
    @Internal
    @NonNull
    public AsmUtilsV2 asmUtils;

    public MethodParametersAsmUtils(@NonNull AsmUtilsV2 asmUtils)
    {
        this.asmUtils = asmUtils;
    }

    @Internal
    @NonNull
    public ExtendedStream<MethodParameterNode> stream(MethodNode method)
    {
        if (method.parameters == null)
            return ExtendedStream.empty();

        var nextParamNumber = new AtomicInteger();

        return ExtendedStream.of(method.parameters)
                .map((param) -> createMethodParameterNode(
                        method,
                        nextParamNumber.getAndIncrement(),
                        param
                )
        );
    }

    private MethodParameterNode createMethodParameterNode(MethodNode method, int paramNumber, ParameterNode param)
    {
        var localVar = method.localVariables.get(Modifier.isStatic(method.access) ? paramNumber : paramNumber + 1);

        var paramComplexNode = new MethodParameterNode();
        paramComplexNode
                .setParameter(param)
                .setVariable(localVar)
                .setParameterNumber(paramNumber)
        ;

        if (method.visibleParameterAnnotations != null)
        {
            var annotations = method.visibleParameterAnnotations[paramNumber];
            if (annotations != null)
            {
                paramComplexNode.getVisibleAnnotations().addAll(annotations);
            }
        }

        if (method.invisibleParameterAnnotations != null)
        {
            var annotations = method.invisibleParameterAnnotations[paramNumber];
            if (annotations != null)
            {
                paramComplexNode.getInvisibleAnnotations().addAll(annotations);
            }
        }

        return paramComplexNode;
    }
}
