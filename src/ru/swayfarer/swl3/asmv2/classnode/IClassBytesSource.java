package ru.swayfarer.swl3.asmv2.classnode;

import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

public interface IClassBytesSource extends IFunction1<String, byte[]>
{
    @Override
    default byte[] applyUnsafe(String s) throws Throwable
    {
        return getBytes(s);
    }

    byte[] getBytes(String className) throws Throwable;
}
