package ru.swayfarer.swl3.asmv2.classnode;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.ClassVisitor;
import ru.swayfarer.swl3.asmv2.asm.FieldVisitor;
import ru.swayfarer.swl3.asmv2.asm.MethodVisitor;
import ru.swayfarer.swl3.asmv2.asm.Type;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.asmv2.asm.tree.FieldNode;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.asmv2.utils.AsmUtilsV2;
import ru.swayfarer.swl3.markers.Internal;

public class NodeClassVisitor extends ClassVisitor
{
    @NonNull
    @Internal
    public AsmUtilsV2 asmUtils;

    @NonNull
    @Internal
    public ClassNode classNode;

    public NodeClassVisitor(int api, AsmUtilsV2 asmUtils, ClassNode classNode)
    {
        this(api, asmUtils, classNode, null);
    }

    public NodeClassVisitor(int api, AsmUtilsV2 asmUtils, ClassNode classNode, ClassVisitor classVisitor)
    {
        super(api, classVisitor);
        this.asmUtils = asmUtils;
        this.classNode = classNode;
    }

    public void visitClassNode(ClassNode classNode)
    {
        super.visit(
                classNode.version,
                classNode.access,
                classNode.name,
                classNode.signature,
                classNode.superName,
                classNode.interfaces == null ? null : classNode.interfaces.toArray(new String[0])
        );
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces)
    {
        visitClassNode(classNode);
    }

    public MethodVisitor visitMethodNode(MethodNode methodNode)
    {
        return super.visitMethod(methodNode.access, methodNode.name, methodNode.desc, methodNode.signature, methodNode.exceptions == null ? null : methodNode.exceptions.toArray(new String[0]));
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions)
    {
        var method = asmUtils.methods().stream(classNode)
                .filter((mn) -> mn.name.equals(name))
                .filter((mn) -> mn.desc.equals(descriptor))
                .findAny()
                .orNull()
        ;

        if (method != null)
            return visitMethodNode(method);

        return super.visitMethod(access, name, descriptor, signature, exceptions);
    }

    public FieldVisitor visitFieldNode(FieldNode fieldNode)
    {
        return super.visitField(fieldNode.access, fieldNode.name, fieldNode.desc, fieldNode.signature, fieldNode.value);
    }

    @Override
    public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value)
    {
        var field = asmUtils.fields().stream(classNode)
                .filter((mn) -> mn.name.equals(name))
                .filter((mn) -> mn.desc.equals(descriptor))
                .findAny()
                .orNull()
        ;

        if (field != null)
            return visitFieldNode(field);

        return super.visitField(access, name, descriptor, signature, value);
    }

    public Type getClassType()
    {
        return Type.getObjectType(classNode.name);
    }
}
