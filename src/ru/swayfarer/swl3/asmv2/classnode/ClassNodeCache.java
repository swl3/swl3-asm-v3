package ru.swayfarer.swl3.asmv2.classnode;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.asmv2.asm.ClassReader;
import ru.swayfarer.swl3.asmv2.asm.tree.ClassNode;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.markers.Internal;

import java.util.concurrent.ConcurrentHashMap;

@Getter
@Setter
@Accessors(chain = true)
public class ClassNodeCache
{
    @Internal
    public IClassBytesSource classBytesSourceFun;

    @Internal
    public ExtendedMap<String, ClassNode> cachedClassNodes = new ExtendedMap<>(new ConcurrentHashMap<>());

    @SneakyThrows
    public ClassNode getClassNode(String className)
    {
        return cachedClassNodes.get(className);
    }

    @SneakyThrows
    public ClassNode getOrCreateClassNode(String className)
    {
        if (className == null)
            return null;

        return getOrCreateClassNode(className, findClassBytes(className));
    }

    @SneakyThrows
    public ClassNode getOrCreateClassNode(String className, byte[] classBytes)
    {
        if (className == null)
            return null;

        return cachedClassNodes.getOrCreate(className, () -> {
            ClassReader classReader = null;

            if (classBytes == null)
            {
                classReader = new ClassReader(className);
            }
            else
            {
                classReader = new ClassReader(classBytes);
            }

            var classNode = new ClassNode();
            classReader.accept(classNode, ClassReader.EXPAND_FRAMES);
            return classNode;
        });
    }

    @SneakyThrows
    public byte[] findClassBytes(String className)
    {
        if (classBytesSourceFun == null)
        {
            return null;
        }

        return classBytesSourceFun.apply(className);
    }
}
