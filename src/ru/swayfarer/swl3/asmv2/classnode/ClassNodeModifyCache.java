package ru.swayfarer.swl3.asmv2.classnode;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.asmv2.asm.Attribute;
import ru.swayfarer.swl3.asmv2.asm.tree.AnnotationNode;
import ru.swayfarer.swl3.asmv2.asm.tree.FieldNode;
import ru.swayfarer.swl3.asmv2.asm.tree.InnerClassNode;
import ru.swayfarer.swl3.asmv2.asm.tree.MethodNode;
import ru.swayfarer.swl3.asmv2.asm.tree.RecordComponentNode;
import ru.swayfarer.swl3.asmv2.asm.tree.TypeAnnotationNode;

import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class ClassNodeModifyCache
{
    /** The runtime visible annotations of this class. May be {@literal null}. */
    public List<AnnotationNode> visibleAnnotations;

    /** The runtime invisible annotations of this class. May be {@literal null}. */
    public List<AnnotationNode> invisibleAnnotations;

    /** The runtime visible type annotations of this class. May be {@literal null}. */
    public List<TypeAnnotationNode> visibleTypeAnnotations;

    /** The runtime invisible type annotations of this class. May be {@literal null}. */
    public List<TypeAnnotationNode> invisibleTypeAnnotations;

    /** The non standard attributes of this class. May be {@literal null}. */
    public List<Attribute> attrs;

    /** The inner classes of this class. */
    public List<InnerClassNode> innerClasses;

    /** The internal names of the nest members of this class. May be {@literal null}. */
    public List<String> nestMembers;

    /** The internal names of the permitted subclasses of this class. May be {@literal null}. */
    public List<String> permittedSubclasses;

    /** The record components of this class. May be {@literal null}. */
    public List<RecordComponentNode> recordComponents;

    /** The fields of this class. */
    public List<FieldNode> fields;

    /** The methods of this class. */
    public List<MethodNode> methods;
}
